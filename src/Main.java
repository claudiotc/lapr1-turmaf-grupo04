import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Main {

    static Scanner read = new Scanner(System.in);
    static final int QUANTIDADE_DE_LINHAS = 1000000;
    static final int QUANTIDADE_DE_COLUNAS_DOS_DADOS = 5; //quantidade de dados a serem tratados: diario nao infetado, acumulado infetado, acumulado hospitalizado, acumulado internadoUCI, acumulado mortes
    static final File FICHEIRODETESTEACUMULADOS = new File("dados.csv");
    static final File FICHEIRODETESTETOTAIS = new File("totalPorEstadoCovid19EmCadaDia.csv");
    static Calendar CALENDARIO1 = new GregorianCalendar(); //declaração de uma variável do tipo calendar que auxilia na identificação dos dias da semana
    static Calendar CALENDARIO2 = new GregorianCalendar();

    public static void main(String[] args) throws IOException, ParseException {
        if (args.length > 0) {

            char escolhaDoTipoDeFicheiro, escolhaDoTipoDeDadosAcumuladosOuTotais;

            String[] listaDasDatas1 = new String[QUANTIDADE_DE_LINHAS],
                    listaDasDatas2 = new String[QUANTIDADE_DE_LINHAS],// array das datas
                    periodoTempo = new String[2];

            int[][] listaDosDados1 = new int[QUANTIDADE_DE_LINHAS][QUANTIDADE_DE_COLUNAS_DOS_DADOS],
                    listaDosDados2 = new int[QUANTIDADE_DE_LINHAS][QUANTIDADE_DE_COLUNAS_DOS_DADOS]; // matriz dos dados covid-19

            int[] listaDosNumerosDosDiasDaSemana1 = new int[QUANTIDADE_DE_LINHAS], listaDosNumerosDosDiasDaSemana2 = new int[QUANTIDADE_DE_LINHAS]; // array do número dos dias da semana (1(domingo)-7(sábado))

            short escolhaDoTipoDeDadosAMostrar;

            int linhaDi = -1, linhaDf = -1, linhaDi1 = -1, linhaDf1 = -1, linhaDi2 = -1, linhaDf2 = -1, linhaT = -1, linhaR = -1;
            for (int i = 0; i < args.length; i++) {
                if (args[i].equals("-r")) {
                    linhaR = i;
                }
                if (args[i].equals("-di")) {
                    linhaDi = i;
                }
                if (args[i].equals("-df")) {
                    linhaDf = i;
                }
                if (args[i].equals("-di1")) {
                    linhaDi1 = i;
                }
                if (args[i].equals("-df1")) {
                    linhaDf1 = i;
                }
                if (args[i].equals("-di2")) {
                    linhaDi2 = i;
                }
                if (args[i].equals("-df2")) {
                    linhaDf2 = i;
                }
                if (args[i].equals("-T")) {
                    linhaT = i;
                }
            }


            File FICHEIROSAIDA = new File(args[linhaT + 2]);
            File FICHEIROTOTAIS = new File(args[linhaT + 2]);
            File MATRIZTRANSICAO = new File(args[linhaT + 2]);
            File FICHEIRODEACUMULADOS = new File(args[linhaT + 2]);


            String formatoDosAcumulados = "dd-MM-yyyy";
            String formatoDosTotais = "dd-MM-yyyy";
            if (linhaDf == -1 && linhaT != -1) {
                FICHEIROTOTAIS = new File(args[linhaT + 2]);
                MATRIZTRANSICAO = new File(args[linhaT + 3]);
                FICHEIROSAIDA = new File(args[linhaT + 4]);
                formatoDosAcumulados = formatoDaData(FICHEIROTOTAIS);
            } else if (linhaT == -1 && linhaDf != -1) {
                FICHEIRODEACUMULADOS = new File(args[linhaDf2 + 2]);
                FICHEIROSAIDA = new File(args[linhaDf2 + 3]);
                formatoDosAcumulados = formatoDaData(FICHEIRODEACUMULADOS);
            } else if (linhaDf != -1) {
                FICHEIROTOTAIS = new File(args[linhaT + 2]);
                FICHEIRODEACUMULADOS = new File(args[linhaT + 3]);
                MATRIZTRANSICAO = new File(args[linhaT + 4]);
                FICHEIROSAIDA = new File(args[linhaT + 5]);
                formatoDosAcumulados = formatoDaData(FICHEIRODEACUMULADOS);
                formatoDosTotais = formatoDaData(FICHEIROTOTAIS);
            }

            final SimpleDateFormat FORMATODOSFICHEIROSDOSACUMULADOS = new SimpleDateFormat(formatoDosAcumulados);
            final SimpleDateFormat FORMATODOFICHEIRODOSTOTAIS = new SimpleDateFormat(formatoDosTotais);
            int linhasOcupadasDosTotais = 0;
            int linhasOcupadasDosAcumulados = 0;

            if (linhaT != -1) {
                linhasOcupadasDosTotais = lerFicheiro(listaDasDatas1, listaDosDados1, listaDosNumerosDosDiasDaSemana1, FICHEIROTOTAIS, FORMATODOFICHEIRODOSTOTAIS);
            }

            if (linhaDf != -1) {
                linhasOcupadasDosAcumulados = lerFicheiro(listaDasDatas2, listaDosDados2, listaDosNumerosDosDiasDaSemana2, FICHEIRODEACUMULADOS, FORMATODOSFICHEIROSDOSACUMULADOS);
            }
            escolhaDoTipoDeDadosAcumuladosOuTotais = 'h';
            escolhaDoTipoDeFicheiro = 'a';

            escolhaDoTipoDeDadosAMostrar = 5;

            if (linhaDf != -1) {
                String[][] periodosDeTempoComparacao2 = calculoDoTamanhoDosIntervalosDaComparativa(listaDasDatas2, linhasOcupadasDosAcumulados, FORMATODOSFICHEIROSDOSACUMULADOS, args, linhaDi1, linhaDf1, linhaDi2, linhaDf2);
                analiseComparativa(periodosDeTempoComparacao2, listaDosDados2, listaDasDatas2, linhasOcupadasDosAcumulados, escolhaDoTipoDeDadosAMostrar, escolhaDoTipoDeFicheiro, escolhaDoTipoDeDadosAcumuladosOuTotais, FICHEIROSAIDA, args);
                System.out.println();
            }


            if (linhaDf != -1) {
                periodoTempo = intervaloDeTempo(listaDasDatas2, linhasOcupadasDosAcumulados, FORMATODOSFICHEIROSDOSACUMULADOS, args, linhaDi, linhaDf);
            }

            char escolhaDoTipoDeAnalise = 'A';
            if (linhaR != -1) {
                escolhaDoTipoDeAnalise = escolhaDoTipoDeAnalise(args, linhaR);
            }

            if (linhaDf != -1) {
                switch (escolhaDoTipoDeAnalise) {
                    case 'A' -> {

                        int[][] analiseDiaria = analiseDiaria(listaDasDatas2, periodoTempo, listaDosDados2, linhasOcupadasDosAcumulados, escolhaDoTipoDeFicheiro, escolhaDoTipoDeDadosAcumuladosOuTotais);
                        String[] datasNoIntervalo = datasPertencentesAoIntervalo(listaDasDatas2, periodoTempo, linhasOcupadasDosAcumulados);
                        mostrarAnaliseEGuardarOuNaoMatrizSingularDupla(datasNoIntervalo, listaDasDatas2, periodoTempo, analiseDiaria, escolhaDoTipoDeDadosAMostrar, linhasOcupadasDosAcumulados, FICHEIROSAIDA, args);

                        System.out.println();
                    }
                    case 'B' -> {
                        if (existeSemanaCompleta(listaDosNumerosDosDiasDaSemana2, listaDasDatas2, periodoTempo[0], periodoTempo[1], linhasOcupadasDosAcumulados)) {
                            analiseSemanasCompletas(escolhaDoTipoDeDadosAcumuladosOuTotais, escolhaDoTipoDeFicheiro, linhasOcupadasDosAcumulados, listaDasDatas2, listaDosDados2, listaDosNumerosDosDiasDaSemana2, periodoTempo[0], periodoTempo[1], escolhaDoTipoDeDadosAMostrar, FICHEIROSAIDA, args);
                            System.out.println();
                        } else {
                            System.out.println("nao existem semanas completas");
                        }
                    }
                    case 'C' -> {
                        if (existeMesesCompletos(listaDasDatas2, linhasOcupadasDosAcumulados, FORMATODOSFICHEIROSDOSACUMULADOS, periodoTempo)) {
                            analiseMesesCompletos(escolhaDoTipoDeFicheiro, escolhaDoTipoDeDadosAcumuladosOuTotais, linhasOcupadasDosAcumulados, listaDasDatas2, listaDosDados2, periodoTempo, FORMATODOSFICHEIROSDOSACUMULADOS, escolhaDoTipoDeDadosAMostrar, FICHEIROSAIDA, args);
                            System.out.println();
                        } else {
                            System.out.println("nao existem meses completos");
                        }
                    }
                }
            }


            if (linhaT != -1) {
                String dataPrevisao = lerEVerificarDataParaPrevisao(FORMATODOFICHEIRODOSTOTAIS, args, linhaT);
                matrizDosDadosPrevistosParaUmDeterminadoDia(FORMATODOFICHEIRODOSTOTAIS, listaDosDados1, listaDasDatas1, linhasOcupadasDosTotais, lerMatrizDasProbabilidades(MATRIZTRANSICAO), dataPrevisao, FICHEIROSAIDA, args);
                System.out.println();

                numeroDeDiasAteAtingirOEstadoDeObito(args, FICHEIROSAIDA, MATRIZTRANSICAO);
                System.out.println();
            }


        } else {
            main2(args);
        }
    }


    public static void main2(String[] args) throws IOException, ParseException {

        boolean[] validacaoDeTodosOsMetodos = testagemDosMetodos();//matriz da verificação dos metodos
        System.out.println("validade dos metodos=> " + Arrays.toString(validacaoDeTodosOsMetodos));
        File ficheiroDosAcumulados, ficheiroDosTotais, ficheiroDosDadosGuardados, ficheiroAUtilizar, ficheiroDaProbabilidades;
        if (verificacaoEscolhaLocalizacaoDoFicheiroALer() == 1) {
            System.out.println("Insira a localização do ficheiro que pretende utlizar (não coloque a localização do ficheiro entre aspas): ");
            read.nextLine();
            String localizacao = read.nextLine();
            ficheiroDosAcumulados = new File(localizacao);
            ficheiroDosTotais = new File(localizacao);
        } else {
            System.out.println("Primeiro insira a localização do ficheiro de dados Acumulados que pretende utlizar (não coloque a localização do ficheiro entre aspas): ");
            read.nextLine();
            String localizacao1 = read.nextLine();
            System.out.println("Agora insira a localização do ficheiro de dados Totais que pretende utlizar (não coloque a localização do ficheiro entre aspas): ");
            String localizacao2 = read.nextLine();
            ficheiroDosAcumulados = new File(localizacao1);
            ficheiroDosTotais = new File(localizacao2);
        }
        System.out.println("Insira a localização do ficheiro csv onde pretende guardar os dados analisados (não coloque a localização do ficheiro entre aspas): ");
        String localizacaoDadosGuardados = read.nextLine();
        ficheiroDosDadosGuardados = new File(localizacaoDadosGuardados);

        char escolhaDoTipoDeFicheiro,
                escolhaAnaliseComparativaOuAnaliseNumIntervalo,
                escolhaDoTipoDeDadosAcumuladosOuTotais;

        String[] listaDasDatas = new String[QUANTIDADE_DE_LINHAS], // array das datas
                periodoTempo;

        int[][] listaDosDados = new int[QUANTIDADE_DE_LINHAS][QUANTIDADE_DE_COLUNAS_DOS_DADOS]; // matriz dos dados covid-19

        int[] listaDosNumerosDosDiasDaSemana = new int[QUANTIDADE_DE_LINHAS]; // array do número dos dias da semana (1(domingo)-7(sábado))

        int linhasOcupadas;

        String formato;

        SimpleDateFormat FORMATO1;

        short escolhaDoTipoDeDadosAMostrar;

        boolean repeticao = true;

        int linhaDi = -1, linhaDf = -1, linhaDi1 = -1, linhaDf1 = -1, linhaDi2 = -1, linhaDf2 = -1, linhaT = -1;

        while (repeticao) {

            escolhaDoTipoDeFicheiro = escolhaDoTipoDeFicheiro();

            if (escolhaDoTipoDeFicheiro == 'a') {
                ficheiroAUtilizar = ficheiroDosAcumulados;
                formato = formatoDaData(ficheiroAUtilizar);
                FORMATO1 = new SimpleDateFormat(formato);
                linhasOcupadas = lerFicheiro(listaDasDatas, listaDosDados, listaDosNumerosDosDiasDaSemana, ficheiroAUtilizar, FORMATO1);
                escolhaAnaliseComparativaOuAnaliseNumIntervalo = escolhaAnaliseComparativaOuAnaliseNumIntervaloParaFicheiroDeAcumulados();
            } else {
                ficheiroAUtilizar = ficheiroDosTotais;
                formato = formatoDaData(ficheiroAUtilizar);
                FORMATO1 = new SimpleDateFormat(formato);
                linhasOcupadas = lerFicheiro(listaDasDatas, listaDosDados, listaDosNumerosDosDiasDaSemana, ficheiroAUtilizar, FORMATO1);
                escolhaAnaliseComparativaOuAnaliseNumIntervalo = escolhaAnaliseComparativaOuAnaliseNumIntervaloParaFicheiroDeTotais();
            }

            if (escolhaAnaliseComparativaOuAnaliseNumIntervalo == 'c') {

                escolhaDoTipoDeDadosAMostrar = escolhaDoTipoDeDadosAMostrar();
                escolhaDoTipoDeDadosAcumuladosOuTotais = escolhaDoTipoDeDadosAcumuladosOuTotais();

                String[][] periodosDeTempoComparacao = calculoDoTamanhoDosIntervalosDaComparativa(listaDasDatas, linhasOcupadas, FORMATO1, args, linhaDi1, linhaDf1, linhaDi2, linhaDf2);
                analiseComparativa(periodosDeTempoComparacao, listaDosDados, listaDasDatas, linhasOcupadas, escolhaDoTipoDeDadosAMostrar, escolhaDoTipoDeFicheiro, escolhaDoTipoDeDadosAcumuladosOuTotais, ficheiroDosDadosGuardados, args);

            } else if (escolhaAnaliseComparativaOuAnaliseNumIntervalo == 'd') {

                escolhaDoTipoDeDadosAMostrar = escolhaDoTipoDeDadosAMostrar();
                escolhaDoTipoDeDadosAcumuladosOuTotais = escolhaDoTipoDeDadosAcumuladosOuTotais();

                System.out.println("Insira o intervalo de datas (com o formato yyyy-MM-dd ou dd-MM-yyyy conforme o formato da data do documento selecionado) em que pretende visualizar esta analise:");
                periodoTempo = intervaloDeTempo(listaDasDatas, linhasOcupadas, FORMATO1, args, linhaDi, linhaDf);


                if (periodoTempo[0].equals(periodoTempo[1])) {

                    analiseDiaria(listaDasDatas, periodoTempo, listaDosDados, linhasOcupadas, escolhaDoTipoDeFicheiro, escolhaDoTipoDeDadosAcumuladosOuTotais);

                } else {

                    char escolhaDoTipoDeAnalise = escolhaDoTipoDeAnalise(args, linhaT);

                    switch (escolhaDoTipoDeAnalise) {

                        case 'A':

                            int[][] analiseDiaria = analiseDiaria(listaDasDatas, periodoTempo, listaDosDados, linhasOcupadas, escolhaDoTipoDeFicheiro, escolhaDoTipoDeDadosAcumuladosOuTotais);
                            String[] datasNoIntervalo = datasPertencentesAoIntervalo(listaDasDatas, periodoTempo, linhasOcupadas);
                            mostrarAnaliseEGuardarOuNaoMatrizSingularDupla(datasNoIntervalo, listaDasDatas, periodoTempo, analiseDiaria, escolhaDoTipoDeDadosAMostrar, linhasOcupadas, ficheiroDosDadosGuardados, args);
                            break;

                        case 'B':

                            if (existeSemanaCompleta(listaDosNumerosDosDiasDaSemana, listaDasDatas, periodoTempo[0], periodoTempo[1], linhasOcupadas)) {
                                analiseSemanasCompletas(escolhaDoTipoDeDadosAcumuladosOuTotais, escolhaDoTipoDeFicheiro, linhasOcupadas, listaDasDatas, listaDosDados, listaDosNumerosDosDiasDaSemana, periodoTempo[0], periodoTempo[1], escolhaDoTipoDeDadosAMostrar, ficheiroDosDadosGuardados, args);
                            } else {
                                System.out.println("nao existem semanas completas");
                            }
                            break;

                        case 'C':

                            if (existeMesesCompletos(listaDasDatas, linhasOcupadas, FORMATO1, periodoTempo)) {
                                analiseMesesCompletos(escolhaDoTipoDeFicheiro, escolhaDoTipoDeDadosAcumuladosOuTotais, linhasOcupadas, listaDasDatas, listaDosDados, periodoTempo, FORMATO1, escolhaDoTipoDeDadosAMostrar, ficheiroDosDadosGuardados, args);
                            } else {
                                System.out.println("nao existem meses completos");
                            }
                            break;

                    }

                }

            } else if (escolhaAnaliseComparativaOuAnaliseNumIntervalo == 'e') {
                System.out.println("Insira a localizacao do Ficheiro das probabilidades das transições (não coloque a localização do ficheiro entre aspas): ");
                read.nextLine();
                String localizacaoProbabilidades = read.nextLine();
                ficheiroDaProbabilidades = new File(localizacaoProbabilidades);
                String dataPrevisao = lerEVerificarDataParaPrevisao(FORMATO1, args, linhaT);
                matrizDosDadosPrevistosParaUmDeterminadoDia(FORMATO1, listaDosDados, listaDasDatas, linhasOcupadas, lerMatrizDasProbabilidades(ficheiroDaProbabilidades), dataPrevisao, ficheiroDosDadosGuardados, args);

            } else {
                System.out.println("Insira a localizacao do Ficheiro das probabilidades das transições (não coloque a localização do ficheiro entre aspas): ");
                read.nextLine();
                String localizacaoProbabilidades = read.nextLine();
                ficheiroDaProbabilidades = new File(localizacaoProbabilidades);
                numeroDeDiasAteAtingirOEstadoDeObito(args, ficheiroDosDadosGuardados, ficheiroDaProbabilidades);
            }
            repeticao = repeticaoDeAnalisesCovid();
        }

    }


    //neste método o utilizador escolhe quantos ficheiros quer carregar, se um ou dois
    public static short verificacaoEscolhaLocalizacaoDoFicheiroALer() {
        short escolha;

        System.out.println("Quantos ficheiros deseja carregar? [1/2]");
        escolha = read.nextShort(); // read.nextShort() lê um short (um número pequeno)

        while (escolha != 1 && escolha != 2) { //caso o utilizador digite um caractere diferente de 1 ou 2, o programa pede de novo a introdução de uma opção válida
            System.out.println("Opcão invalida. Tente novamente"); //mensagens que aparecem caso o que o utilizador inseriu não seja válido
            System.out.print("Opção que pretende escolher: ");
            escolha = read.nextShort();
        }

        return escolha;
    }

    //método que pergunta ao utilizador se ele quer realizar (ou não) uma nova análise. Se quiser seleciona "S", se não seleciona "N"
    public static boolean repeticaoDeAnalisesCovid() {

        System.out.print("Deseja realizar novas análises aos dados de Covid-19 ? [S/N] \n Opção que pretende escolher: ");
        char escolha = read.next().charAt(0); //leitura da opção que o utilizador deseja "read.next().charAt(0)" lê apenas um caractere

        while (escolha != 'S' && escolha != 'N') { //verificação que garante que o utilizador só digita S ou N (se o utilizador inserir os caracteres em miniscula não é considerado válido)

            System.out.println("Opcão invalida. Tente novamente"); //mensagens que aparecem caso o que o utilizador inseriu não seja válido
            System.out.print("Opção que pretende escolher: ");
            escolha = read.next().charAt(0);

        }

        return escolha == 'S'; //caso a escolha seja S, é retornado true que permite a reinicilização do programa. Caso a pessoa escolha N é retornado false que incerra o programa

    }

    //método para o utilizador escolher qual o tipo de ficheiro que quer utilizar
    public static char escolhaDoTipoDeFicheiro() {

        System.out.print("Escolha a opção do ficheiro que deseja analisar: \n a) Ficheiro de acumulação de dados \n b) Ficheiro de dados total \n Opção que pretende escolher: ");
        char escolha = read.next().charAt(0); //leitura da opção que o utilizador deseja "read.next().charAt(0)" lê apenas um caractere

        while (escolha != 'a' && escolha != 'b') { //caso o utilizador insira um caractere diferente de 'a' ou 'b' o programa torna a pedir que o utilizador insira uma opção válida
            System.out.println("Opcão invalida. Tente novamente"); //mensagens que aparecem quando o utilizador não insere um caractere válido
            System.out.print("Opção que pretende escolher: ");
            escolha = read.next().charAt(0);
        }

        return escolha;

    }

    //método para o utilizador escolher qual o tipo de dados que quer tratar
    public static char escolhaDoTipoDeDadosAcumuladosOuTotais() {
        System.out.print("Escolha a opção relativa ao tipo de dados a tratar: \n g) Dados Acumulados \n h) Dados Totais \n Opção que pretende escolher: ");
        char escolha = read.next().charAt(0);  //leitura da opção que o utilizador deseja "read.next().charAt(0)" lê apenas um caractere

        while (escolha != 'g' && escolha != 'h') { //caso o utilizador insira um caractere diferente de 'g' ou 'h' o programa torna a pedir que o utilizador insira uma opção válida
            System.out.println("Opcão invalida. Tente novamente"); //mensagens que aparecem quando o utilizador não insere um caractere válido
            System.out.print("Opção que pretende escolher: ");
            escolha = read.next().charAt(0);
        }

        return escolha;
    }

    //método para o utilizador escolher qual o tipo de análise que quer fazer caso o utilizador escolha o tipo de dados acumulados
    public static char escolhaAnaliseComparativaOuAnaliseNumIntervaloParaFicheiroDeAcumulados() {

        System.out.print("Escolha a opção relativa ao tipo de período que deseja analisar: \n c) Análise Comparativa \n d) Análise num Intervalo \n Opção que pretende escolher: ");
        char escolha = read.next().charAt(0); //leitura da opção que o utilizador deseja "read.next().charAt(0)" lê apenas um caractere

        while (escolha != 'c' && escolha != 'd') { //caso o utilizador insira um caractere diferente de 'c' ou 'd' o programa torna a pedir que o utilizador insira uma opção válida
            System.out.println("Opcão invalida. Tente novamente"); //mensagens que aparecem quando o utilizador não insere um caractere válido
            System.out.print("Opção que pretende escolher: ");
            escolha = read.next().charAt(0);
        }

        return escolha;
    }

    //método para o utilizador escolher qual o tipo de análise que quer fazer caso o utilizador escolha o tipo de dados totais
    public static char escolhaAnaliseComparativaOuAnaliseNumIntervaloParaFicheiroDeTotais() {

        System.out.print("Escolha a opção relativa ao tipo de período que deseja analisar: \n c) Análise Comparativa \n d) Análise num Intervalo \n e) Efetuar uma previsão \n f) Dias até à morte \n Opção que pretende escolher: ");
        char escolha = read.next().charAt(0); //leitura da opção que o utilizador deseja "read.next().charAt(0)" lê apenas um caractere

        while (escolha != 'c' && escolha != 'd' && escolha != 'e' && escolha != 'f') { //caso o utilizador insira um caractere diferente de 'd' ou 'e' ou 'f' o programa torna a pedir que o utilizador insira uma opção válida
            System.out.println("Opcão invalida. Tente novamente"); //mensagens que aparecem quando o utilizador não insere um caractere válido
            System.out.print("Opção que pretende escolher: ");
            escolha = read.next().charAt(0);
        }

        return escolha;
    }

    //caso o utilizador queira analisar num período aparece-lhe qual o tipo de período que quer: diário, semanal, mensal
    public static char escolhaDoTipoDeAnalise(String[] args, int linhaR) {

        boolean isValid = false; //este boolean serve para perceber quando é colocado um caractere de entre as opções, fica true quando entra num dos 3 "cases" (A, B e C)


        if (args.length == 0) {
            System.out.print("Escolha uma das seguintes opções: \n A) Análise Diária \n B) Análise Semanal \n C) Análise Mensal \n Opção que pretende escolher: ");
        }
        char escolha; //serve para ler apenas um caractere

        if (linhaR == -1) {
            escolha = read.next().charAt(0);
        } else {
            escolha = args[linhaR + 1].charAt(0);
        }

        while (!isValid) { //o ! nega ou seja enquanto o boolean isValid for falso ele repete o switch case que lá está dentro

            switch (escolha) {
                case 'A', 'B', 'C' -> isValid = true;
                default -> { //sempre que a "escolha" entra no "default" o boolean isValid não fica verdadeiro, é lido de novo o caractere e o while repete e faz de novo a verificação
                    System.out.println("Opcão invalida. Tente novamente");
                    System.out.print("Opção que pretende escolher: ");
                    escolha = read.next().charAt(0);
                }
            }

        }

        return escolha; //retorna a opção que o utilizador escolheu

    }

    //
    public static short escolhaDoTipoDeDadosAMostrar() {

        boolean isValid = false;

        System.out.print("Escolha uma das seguintes opções: \n 0) Análise dos não infetados \n 1) Análise dos infetados \n 2) Análise dos hospitalizados \n 3) Análise dos internados \n 4) Análise das mortes \n 5) Todas as opções \n Opção que pretende escolher: ");
        short escolha = read.nextShort();

        while (!isValid) {

            switch (escolha) {
                case 0, 1, 2, 3, 4, 5 -> isValid = true;
                default -> {
                    System.out.println("Opcão invalida. Tente novamente");
                    System.out.print("Opção que pretende escolher: ");
                    escolha = read.nextShort();
                }
            }

        }

        return escolha;

    }


    //este método tem a função de imprimir e guardar (ou não) as análises com duas datas (no caso da análise semanal e mensal em que são colocadas as datas de inicío e fim de cada período de tempo)
    public static void mostrarAnaliseEGuardarOuNaoMatrizDuplaDupla(String textoIndicador, String textoIndicadorCSV, String texto1, String texto2, String textoNoCSV1, String textoNoCSV2, String[][] datasNoIntervalo, String[] args, int[][] analiseDosDados, short escolha, int linhasOcupadas, File ficheiroDosDadosGuardados) throws IOException {

        System.out.println(textoIndicador);
        if (escolha == 5) { //se o utilizador escolher 5 são apresentados todos os dados relativamente ao período
            System.out.println(texto1); //como este método é utilizado para mais do que uma vez o texto passa por parametro
            escreverMatrizStringDuplainteiroDupla(datasNoIntervalo, analiseDosDados, linhasOcupadas);
        } else { //caso o utilizador escolha qualquer uma das outras opções só é apresentado esse dado
            System.out.println(texto2);
            escreverMatrizStringDuplainteiroSingular(datasNoIntervalo, analiseDosDados, linhasOcupadas, escolha); //a escolha entra para o método, pois cada número diz respeito a uma coluna e a um tipo de dado
        }


        if (guardarDados(args)) { //caso a pessoa tenha selecionado S no método guardarDados, a condição é veradeira e o if realizasse, guardando assim a informação que o utilizador pediu ver num ficheiro CSV
            if (escolha == 5) {
                guardarEscrevermatrizStringinteirosDuplaVirgulas(textoIndicadorCSV, textoNoCSV1, datasNoIntervalo, analiseDosDados, linhasOcupadas, ficheiroDosDadosGuardados);
            } else {
                guardarEscrevermatrizStringinteirosSingularVirgulas(textoIndicadorCSV, textoNoCSV2, datasNoIntervalo, analiseDosDados, linhasOcupadas, escolha, ficheiroDosDadosGuardados);
            }
        }


    }

    //este método tem a função de imprimir e guardar (ou não) as análises com uma data (análise diária em que só é apresentada a data de cada dia dentro dos limites do período inserido pelo utilizador)
    public static void mostrarAnaliseEGuardarOuNaoMatrizSingularDupla(String[] datasNoIntervalo, String[] listaDasDatas, String[] periodoTempo, int[][] analiseDiaria, short escolha, int linhasOcupadas, File ficheiroDosDadosGuardados, String[] args) throws IOException {

        int[] posicaoDasDatas = posicaoDataInicialEFinal(listaDasDatas, periodoTempo[0], periodoTempo[1], linhasOcupadas); //é enviado para dentro do método as duas datas que o utilizador inseriu para limitarem o intervalo de dados a aparecer. São retornadas as suas posições no documento e consequentemente a posição dos dados.
        int linhasTotaisEntreDatas = (posicaoDasDatas[1] - posicaoDasDatas[0] + 1); /*é calculado o número de linhas entre as linhas das datas inseridas, ou seja o número de dias que compõe o intervalo inserido
        Este inteiro serve para o ciclo for dos métodos de impressao */
        System.out.println("Análise Diária");
        if (escolha == 5) {
            System.out.println("            Data     NaoInfetados        Infetados   Hospitalizados    InternadosUCI           Obitos"); //Sempre que este método é utilizado o texto é o mesmo, logo não há necessidade que este seja passado por parâmetro
            escreverMatrizStringSingularInteiroDupla(datasNoIntervalo, analiseDiaria, linhasTotaisEntreDatas);
        } else {
            System.out.println("            Data    DadoAnalisado");
            escreverMatrizStringSingularInteiroSingular(datasNoIntervalo, analiseDiaria, linhasTotaisEntreDatas, escolha);
        }


        if (guardarDados(args)) {
            if (escolha == 5) {
                guardarEscreverMatrizStringSingularInteiroDuplo(datasNoIntervalo, analiseDiaria, linhasTotaisEntreDatas, ficheiroDosDadosGuardados);
            } else {
                guardarEscreverMatrizStringSingularInteiroSingular(datasNoIntervalo, analiseDiaria, linhasTotaisEntreDatas, escolha, ficheiroDosDadosGuardados);
            }
        }


    }

    //aqui são comparados os dois intervalos que vão ser analisados na "Comparativa", o tamanho do intervalo mais pequeno é o utilizado. Os dias do intervalo com mais dias, que excedem são descartados
    public static String[][] calculoDoTamanhoDosIntervalosDaComparativa(String[] listaDasDatas, int linhasOcupadas, SimpleDateFormat formato, String[] args, int linhaDi1, int linhaDf1, int linhaDi2, int linhaDf2) throws ParseException {

        String[][] periodosDeTempoComparacao = new String[2][3]; //este array serve para guardar a data inicial e final de cada período e o número de dias pelo que é constituído cada um

        int diasPeriodoTempo1, diasPeriodoTempo2;

        String[] periodoTempo1, //array bidimensional que guarda a data inicial e final inseridas pelo utilizador para o primeiro período
                periodoTempo2; //array bidimensional que guarda a data inicial e final inseridas pelo utilizador para o segundo período

        int[] posicaoFinalInicial1,
                posicaoFinalInicial2;


        if (linhaDi1 == -1) {
            System.out.println("Insira o primeiro intervalo de datas (com o formato yyyy-MM-dd ou dd-MM-yyyy conforme o formato da data do documento selecionado) em que pretende visualizar esta analise comparativa: ");
        }
        periodoTempo1 = intervaloDeTempo(listaDasDatas, linhasOcupadas, formato, args, linhaDi1, linhaDf1); //neste método são lidas as duas datas que limitam o primeiro período de tempo a analisar
        if (linhaDi1 == -1) {
            System.out.println("Insira agora o segundo intervalo de datas:");
        }
        periodoTempo2 = intervaloDeTempo(listaDasDatas, linhasOcupadas, formato, args, linhaDi2, linhaDf2);
        posicaoFinalInicial1 = posicaoDataInicialEFinal(listaDasDatas, periodoTempo1[0], periodoTempo1[1], linhasOcupadas);
        posicaoFinalInicial2 = posicaoDataInicialEFinal(listaDasDatas, periodoTempo2[0], periodoTempo2[1], linhasOcupadas); //após serem lidas as datas estas são enviadas para o método que retorna a sua posição no documento
        diasPeriodoTempo1 = (posicaoFinalInicial1[1] - posicaoFinalInicial1[0]) + 1; //sabendo as posições das duas datas que limitam cada período, é calculado o número de dias entre elas
        diasPeriodoTempo2 = (posicaoFinalInicial2[1] - posicaoFinalInicial2[0]) + 1;

        if (diasPeriodoTempo1 > diasPeriodoTempo2) { //se o período de tempo1 for maior que o período de tempo2, assume-se o número de dias do período de tempo2 já que é o mais pequeno
            periodosDeTempoComparacao[0][2] = Integer.toString(diasPeriodoTempo2); //para o número de dias dos períodos ser guardado junto às datas este é colocado em string
            periodosDeTempoComparacao[1][2] = Integer.toString(diasPeriodoTempo2);
        } else { //se o período de tempo2 for maior que o período de tempo1, assume-se o número de dias do período de tempo1 já que no primeiro caso é o mais pequeno e se forem iguais o numero de dias dos dois períodos é igual e por isso indiferente
            periodosDeTempoComparacao[0][2] = Integer.toString(diasPeriodoTempo1);
            periodosDeTempoComparacao[1][2] = Integer.toString(diasPeriodoTempo1);
        }

        periodosDeTempoComparacao[0][0] = periodoTempo1[0]; //é guardada a data inicial do primeiro período no array
        periodosDeTempoComparacao[0][1] = periodoTempo1[1]; //é guardada a data final do primeiro período no array
        periodosDeTempoComparacao[1][0] = periodoTempo2[0]; //é guardada a data inicial do segundo período no array
        periodosDeTempoComparacao[1][1] = periodoTempo2[1]; //é guardada a data final do segundo período no array


        return periodosDeTempoComparacao;

    }


    public static void analiseComparativa(String[][] periodosDeTempoComparacao, int[][] listaDosDados, String[] listaDasDatas, int linhasOcupadas, short escolhaDoTipoDeAnalise, char escolhaFicheiro, char escolhaDoTipoDeDadosAcumuladosOuTotais, File ficheiroDosDadosGuardados, String[] args) throws IOException {

        int totalDeDias = Integer.parseInt(periodosDeTempoComparacao[0][2]); //no método calculoDoTamanhoDosIntervalosDaComparativa é retornada uma matriz com as datas que limitam os períodos e o número de dias a considerar, o número de dias do período menor ou se forem iguais o de qualquer um, este é transformado em inteiro para efeitos de utilização em ciclos for e de tamanhos em arrays

        String[] periodoTempo1 = periodosDeTempoComparacao[0], //as duas datas que limitam o primeiro período são guardadas de forma independente
                periodoTempo2 = periodosDeTempoComparacao[1], //as duas datas que limitam o segundo período são guardadas de forma independente
                datasNoIntervalo1 = datasPertencentesAoIntervalo(listaDasDatas, periodoTempo1, linhasOcupadas), //neste array são guardadas todas as datas pertencentes ao primeiro intervalo
                datasNoIntervalo2 = datasPertencentesAoIntervalo(listaDasDatas, periodoTempo2, linhasOcupadas); //neste array são guardadas todas as datas pertencentes ao segundo intervalo

        String[][] datasDosIntervalosDeTempo = new String[totalDeDias][2]; //guarda as datas pertencentes aos dois períodos num mesmo sitio para posteriormente serem imprensas na apresentação de dados

        for (int linha = 0; linha < totalDeDias; linha++) { //copia as datas que estão em cada array de cada período de tempo para uma das colunas da matriz datasDosIntervalosDeTempo: o primeiro período ocupada a primeira coluna e o segundo a segunda

            datasDosIntervalosDeTempo[linha][0] = datasNoIntervalo1[linha];
            datasDosIntervalosDeTempo[linha][1] = datasNoIntervalo2[linha];

        }

        //no método análise diária são reunidos os dados diários de cada período e guardados em dois arrays separados
        int[][] analiseDiaria1 = analiseDiaria(listaDasDatas, periodoTempo1, listaDosDados, linhasOcupadas, escolhaFicheiro, escolhaDoTipoDeDadosAcumuladosOuTotais),
                analiseDiaria2 = analiseDiaria(listaDasDatas, periodoTempo2, listaDosDados, linhasOcupadas, escolhaFicheiro, escolhaDoTipoDeDadosAcumuladosOuTotais),
                analiseComparativa = new int[totalDeDias][QUANTIDADE_DE_COLUNAS_DOS_DADOS],
                analiseTodosPeriodosEComparativa = new int[totalDeDias][QUANTIDADE_DE_COLUNAS_DOS_DADOS * 3];

        //num novo array é guardada a diferença entre cada dado de cada duas datas que ocupem a mesma posição em cada intervalo
        for (int linha = 0; linha < totalDeDias; linha++) {

            for (int coluna = 0; coluna < QUANTIDADE_DE_COLUNAS_DOS_DADOS; coluna++) {

                analiseComparativa[linha][coluna] = analiseDiaria2[linha][coluna] - analiseDiaria1[linha][coluna];

            }
        }

        //num novo array são guardados os dados referentes ao primeiro período, ao segundo e à sua subtração
        for (int linha = 0; linha < totalDeDias; linha++) {
            System.arraycopy(analiseDiaria1[linha], 0, analiseTodosPeriodosEComparativa[linha], 0, QUANTIDADE_DE_COLUNAS_DOS_DADOS);
            //nas primeiras 5 colunas (contando com a posição 0) são guardados os dados do primeiro intervalo, e por isso o ciclo for começa em 5 para guardar os dados do segundo período
            System.arraycopy(analiseDiaria2[linha], 0, analiseTodosPeriodosEComparativa[linha], 5, QUANTIDADE_DE_COLUNAS_DOS_DADOS + 5 - 5);
            //nas primeiras 10 colunas (contando com a posição 0) são guardados os dados dos primeiro e segundo intervalos, e por isso o ciclo for começa em 10 para guardar os dados da subtração
            System.arraycopy(analiseComparativa[linha], 0, analiseTodosPeriodosEComparativa[linha], 10, QUANTIDADE_DE_COLUNAS_DOS_DADOS + 10 - 10);
        }

        String[] textoDaMediaEDesvio = new String[]{"Media", "Desvio Padrao"}; //texto que passa por parâmetro para ser impresso
        final int tamanhoDoDesvioEMedia = 2;

        double[][] mediaEDesvio = new double[tamanhoDoDesvioEMedia][QUANTIDADE_DE_COLUNAS_DOS_DADOS * 3]; //array que guarda os valores relativos à média e ao desvio padrão, a QUANTIDADE_DE_COLUNAS_DOS_DADOS é multiplicada por 3 porque o mesmo parametro (tipo de dado) aparece 3 vezes uma para cada um dos dois períodos e outra para a subtração
        mediaEDesvio[0] = mediaDaAnaliseComparativa(analiseTodosPeriodosEComparativa, totalDeDias);
        mediaEDesvio[1] = desvioPadraoDaAnalise(mediaEDesvio[0], analiseTodosPeriodosEComparativa, totalDeDias);


        if (escolhaDoTipoDeAnalise == 5) {
            escreverMatrizDosDadosDaComparacaoAnaliseCompleta(datasDosIntervalosDeTempo, analiseTodosPeriodosEComparativa, totalDeDias, mediaEDesvio, textoDaMediaEDesvio, tamanhoDoDesvioEMedia, ficheiroDosDadosGuardados, args);
        } else {
            escreverMatrizDosDadosDaComparacaoDaAnaliseParaUmDado(datasDosIntervalosDeTempo, analiseTodosPeriodosEComparativa, totalDeDias, tamanhoDoDesvioEMedia, textoDaMediaEDesvio, mediaEDesvio, ficheiroDosDadosGuardados, args);
        }

    }

    //método que imprime os dados da comparativa: datas, dados, média e desvio padrão
    public static void escreverMatrizDosDadosDaComparacaoAnaliseCompleta(String[][] datas, int[][] dados, int linhasOcupadasPrimeiro, double[][] mediaEDesvio, String[] textoDaMediaEDesvio, int linhasOcupadasSegundo, File ficheiroDosDadosGuardados, String[] args) throws IOException {
        System.out.println("Análise Comparativa");
        System.out.println("     1.º Período        NaoInfetados           Infetados      Hospitalizados       InternadosUCI              Obitos|     2.º Período        NaoInfetados           Infetados      Hospitalizados       InternadosUCI              Obitos|    Sub-NaoInfetados       Sub-Infetados  Sub-Hospitalizados   Sub-InternadosUCI          Sub-Obitos");

        for (int linha = 0; linha < linhasOcupadasPrimeiro; linha++) {
            for (int coluna = 0; coluna < 17; coluna++) {
                if (coluna == 0) {
                    System.out.printf("%16s", datas[linha][coluna]);
                } else if (coluna == 6) {
                    System.out.printf("%16s", datas[linha][coluna - 5]);
                } else if (coluna < 6) {
                    System.out.printf("%20d", dados[linha][coluna - 1]);
                } else {
                    System.out.printf("%20d", dados[linha][coluna - 2]);
                }
                if (coluna == 5 || coluna == 11) {
                    System.out.print("|");
                }

            }
            System.out.println();
        }

        for (int linha = 0; linha < linhasOcupadasSegundo; linha++) {
            for (int coluna = 0; coluna < 17; coluna++) {
                if (coluna == 0 || coluna == 6) {
                    System.out.printf("%16s", textoDaMediaEDesvio[linha]);
                } else if (coluna < 6) {
                    System.out.printf("%20.4f", mediaEDesvio[linha][coluna - 1]);
                } else {
                    System.out.printf("%20.4f", mediaEDesvio[linha][coluna - 2]);
                }
                if (coluna == 5 || coluna == 11) {
                    System.out.print("|");
                }

            }
            System.out.println();

        }

        //texto introdutório que passa por parâmetro caso o utilizador pretenda que seja criado um ficheiro CSV
        String textoIntrodutorio = "1. Periodo,NaoInfetados,Infetados,Hospitalizados,InternadosUCI,Obitos,2. Periodo,NaoInfetados,Infetados,Hospitalizados,InternadosUCI,Obitos,Sub-NaoInfetados,Sub-Infetados,Sub-Hospitalizados,Sub-InternadosUCI,Sub-Obitos";
        String textoIndicador = "Analise Comparativa";


        if (guardarDados(args)) {
            guardarMatrizDosDadosDaComparacaoAnaliseCompleta(textoIndicador, linhasOcupadasPrimeiro, datas, dados, mediaEDesvio, textoDaMediaEDesvio, linhasOcupadasSegundo, textoIntrodutorio, ficheiroDosDadosGuardados);
        }

    }

    //método que guarda os dados relativos à análise comparativa num documento CSV
    public static void guardarMatrizDosDadosDaComparacaoAnaliseCompleta(String textoIndicador, int linhasOcupadasPrimeiro, String[][] datas, int[][] dados, double[][] mediaEDesvio, String[] textoDaMediaEDesvio, int linhasOcupadasSegundo, String textoIntrodutorio, File ficheiroDosDadosGuardados) throws IOException {
        FileWriter writeOnFile = new FileWriter((ficheiroDosDadosGuardados), true);
        writeOnFile.write(textoIndicador + "\n");
        writeOnFile.write(textoIntrodutorio + "\n");
        for (int linha = 0; linha < linhasOcupadasPrimeiro; linha++) {
            for (int coluna = 0; coluna < 17; coluna++) {
                if (coluna == 0) {
                    writeOnFile.write(datas[linha][coluna] + ",");
                } else if (coluna == 6) {
                    writeOnFile.write(datas[linha][coluna - 5] + ",");
                } else if (coluna < 6) {
                    writeOnFile.write(dados[linha][coluna - 1] + ",");
                } else if (coluna == 16) {
                    writeOnFile.write(dados[linha][coluna - 2] + "");
                } else {
                    writeOnFile.write(dados[linha][coluna - 2] + ",");
                }
            }
            writeOnFile.write("\n");
        }

        for (int linha = 0; linha < linhasOcupadasSegundo; linha++) {
            for (int coluna = 0; coluna < 17; coluna++) {
                if (coluna == 0 || coluna == 6) {
                    writeOnFile.write(textoDaMediaEDesvio[linha] + ",");
                } else if (coluna < 6) {
                    writeOnFile.write(String.format(Locale.US, "%.4f,", mediaEDesvio[linha][coluna - 1]));
                } else if (coluna == 16) {
                    writeOnFile.write(String.format(Locale.US, "%.4f", mediaEDesvio[linha][coluna - 2]));
                } else {
                    writeOnFile.write(String.format(Locale.US, "%.4f,", mediaEDesvio[linha][coluna - 2]));
                }
            }
            writeOnFile.write("\n");
        }
        writeOnFile.close();
    }

    //método que imprime APENAS UM dado da comparativa escolhido pelo utilizador
    public static void escreverMatrizDosDadosDaComparacaoDaAnaliseParaUmDado(String[][] datas, int[][] dados, int linhasOcupadasPrimeiro, int linhasOcupadasSegundo, String[] textoMediaEDesvio, double[][] mediaEDesvio, File ficheiroDosDadosGuardados, String[] args) throws IOException {
        
        System.out.println("Análise Comparativa");
        System.out.println("     1.º Período      Dado Analisado     2.º Período      Dado Analisado       Sub-Analisado");

        for (int linha = 0; linha < linhasOcupadasPrimeiro; linha++) {
            for (int coluna = 0; coluna < 5; coluna++) {
                if (coluna == 0) {
                    System.out.printf("%16s", datas[linha][coluna]);
                } else if (coluna == 2) {
                    System.out.printf("%16s", datas[linha][coluna - 1]);
                } else if (coluna == 1) {
                    System.out.printf("%20d", dados[linha][coluna - 1]);
                } else {
                    System.out.printf("%20d", dados[linha][coluna - 2]);
                }
            }
            System.out.println();
        }

        for (int linha = 0; linha < linhasOcupadasSegundo; linha++) {
            for (int coluna = 0; coluna < 5; coluna++) {
                if (coluna == 0 || coluna == 2) {
                    System.out.printf("%16s", textoMediaEDesvio[linha]);
                } else if (coluna == 1) {
                    System.out.printf("%20.4f", mediaEDesvio[linha][coluna - 1]);
                } else {
                    System.out.printf("%20.4f", mediaEDesvio[linha][coluna - 2]);
                }
            }
            System.out.println();
        }
        String textoIntrodutorio = "1. Periodo,Dado Analisado,2. Periodo,Dado Analisado,Sub-Analisado";
        String textoIndicador = "Analise Comparativa";

        if (guardarDados(args)) {
            guardarMatrizDosDadosDaComparacaoAnaliseParaUmDado(textoIndicador, linhasOcupadasPrimeiro, linhasOcupadasSegundo, datas, dados, mediaEDesvio, textoMediaEDesvio, textoIntrodutorio, ficheiroDosDadosGuardados);
        }

    }

    //método que guarda APENAS UM dado da comparativa escolhido pelo utilizador num ficheiro CSV, caso se pretenda
    public static void guardarMatrizDosDadosDaComparacaoAnaliseParaUmDado(String textoIndicador, int linhasOcupadasPrimeiro, int linhasOcupadasSegundo, String[][] datas, int[][] dados, double[][] mediaEDesvio, String[] textoDaMediaEDesvio, String textoIntrodutorio, File ficheiroDosDadosGuardados) throws IOException {
        FileWriter writeOnFile = new FileWriter((ficheiroDosDadosGuardados), true);
        writeOnFile.write(textoIndicador + "\n");
        writeOnFile.write(textoIntrodutorio + "\n");
        for (int linha = 0; linha < linhasOcupadasPrimeiro; linha++) {
            for (int coluna = 0; coluna < 5; coluna++) {
                if (coluna == 0) {
                    writeOnFile.write(datas[linha][coluna] + ",");
                } else if (coluna == 1) {
                    writeOnFile.write(dados[linha][coluna - 1] + ",");
                } else if (coluna == 2) {
                    writeOnFile.write(datas[linha][coluna - 1] + ",");
                } else if (coluna == 3) {
                    writeOnFile.write(dados[linha][coluna - 2] + ",");
                } else {
                    writeOnFile.write(dados[linha][coluna - 2] + "");
                }
            }
            writeOnFile.write("\n");
        }

        for (int linha = 0; linha < linhasOcupadasSegundo; linha++) {
            for (int coluna = 0; coluna < 5; coluna++) {
                if (coluna == 0 || coluna == 2) {
                    writeOnFile.write(textoDaMediaEDesvio[linha] + ",");
                } else if (coluna == 1) {
                    writeOnFile.write(String.format(Locale.US, "%.4f,", mediaEDesvio[linha][coluna - 1]));
                } else {
                    writeOnFile.write(String.format(Locale.US, "%.4f,", mediaEDesvio[linha][coluna - 2]));
                }
            }
            writeOnFile.write("\n");
        }
        writeOnFile.close();
    }

    //a média é feita para cada parâmetro de cada período e para cada parâmetro da subtração entre os dados dos dois períodos
    public static double[] mediaDaAnaliseComparativa(int[][] analiseTodosPeriodosEComparativa, int totalDeDias) {
        //array que vai armazenar a média de cada parâmetro, cada média ficará na posição que a coluna a que se refere ocupa na matriz analiseTodosPeriodosEComparativa
        double[] mediaDaAnaliseComparativa = new double[QUANTIDADE_DE_COLUNAS_DOS_DADOS * 3];

        //ciclo for que corre as colunas da matriz dos dados da comparativa
        for (int coluna = 0; coluna < QUANTIDADE_DE_COLUNAS_DOS_DADOS * 3; coluna++) {

            int soma = 0; //variável que acumula a soma de todos os dados de uma coluna

            for (int linha = 0; linha < totalDeDias; linha++) { //ciclo for que corre as linhas da matriz dos dados da comparativa, ou seja junta os valores todos de vários dias mas de apenas um dado

                soma += analiseTodosPeriodosEComparativa[linha][coluna];

            }

            mediaDaAnaliseComparativa[coluna] = (double) soma / totalDeDias; //após somar todos os números de uma coluna faz-se a média, ou seja divide-se pelo número de elementos dessa coluna

        }

        return mediaDaAnaliseComparativa;
    }

    //assim como a média, o desvio padrão é feito para cada parâmetro de cada período e para cada parâmetro da subtração entre os dados dos dois períodos
    public static double[] desvioPadraoDaAnalise(double[] mediaDaAnaliseComparativa, int[][] analiseTodosPeriodosEComparativa, int totalDeDias) {
        //array que vai armazenar o desvio padrão de cada parâmetro, cada média ficará na posição que a coluna a que se refere ocupa na matriz analiseTodosPeriodosEComparativa
        double[] desvioPadraoDaAnalise = new double[QUANTIDADE_DE_COLUNAS_DOS_DADOS * 3];

        for (int coluna = 0; coluna < QUANTIDADE_DE_COLUNAS_DOS_DADOS * 3; coluna++) {
            double auxiliar = 0;
            //para fazer o desvio padrão primeiro retira-se a cada um dos dados de cada coluna o valor da média dessa mesma a coluna e fz-se o seu quadrado, à medida que se avança nas linhas da coluna junta-se todas as subtrações (ao quadrado)
            for (int linha = 0; linha < totalDeDias; linha++) {
                auxiliar += Math.pow((analiseTodosPeriodosEComparativa[linha][coluna] - mediaDaAnaliseComparativa[coluna]), 2);
            }
            desvioPadraoDaAnalise[coluna] = Math.sqrt(auxiliar / totalDeDias); //quando já se tem a soma de todos os cálculos dos valores da coluna, faz-se a raiz quadrada da soma a dividir pelo número de linhas (corresponde ao número de dias)
        }
        return desvioPadraoDaAnalise;
    }

    public static String formatoDaData(File ficheiro) throws FileNotFoundException {

        String formato; //string que retorna o formato da data do documento que o utilizador pretende usar
        Scanner readFromFile = new Scanner(ficheiro); //o documento que o utilizador quer usar é consultado
        readFromFile.nextLine(); //a primeira linha do documento é ignorada por ser um texto introdutório "data,diario_nao_infetado,acumulado_infetado,acumulado_hospitalizado,acumulado_internadoUCI,acumulado_morte"

        String[] dadosDaDataSeparados = readFromFile.next().split("-"); //o primeiro dado que aparece em cada linha do documento é a data e o que divide o ano, o mês e o dia é o "-"
        readFromFile.close();

        if (dadosDaDataSeparados[0].length() == 2) { //dadosDaDataSeparados[0].length() conta o número de caracteres contidos nessa posição, no caso de ser o dia primeiro vão ser dois caracteres, caso seja ano contará com 4 caracteres
            formato = "dd-MM-yyyy"; //formato da data (dd-MM-yyyy)
        } else {
            formato = "yyyy-MM-dd"; //formato da data (yyyy-MM-dd)
        }

        return formato;

    }


    public static int lerFicheiro(String[] matrizDasDatas, int[][] matrizDosDados, int[] matrizDosNumerosDosDiasDaSemana, File Ficheiro, SimpleDateFormat formato) throws FileNotFoundException, ParseException {

        Scanner readFromFile = new Scanner(Ficheiro); //scanner para ler a informação contida no ficheiro (FICHEIRO) declarado como constante global
        String textoDosDados; //"String" que irá guardar uma linha completa do documento
        String[] dadosDeUmDia; //array auxiliar que irá guardar cada dado do textoDosDados numa posição, ou seja, a linha ficará dividida
        int linha = 0; //variável que avança nas linhas dos arrays, impedindo que a informação de uma linha seja guardada onde esteja informação de outra

        readFromFile.nextLine(); //descarta a primeira linha do documento

        while (readFromFile.hasNext()) { //enquanto houver linhas para ler no documento o ciclo irá repetir-se
            textoDosDados = readFromFile.nextLine(); //a variável guarda toda a frase da linha
            dadosDeUmDia = textoDosDados.split(","); //a frase guardada em textoDosDados é separada (sendo o elemento de separação a vírgula) e os dados são guardados individualmente em cada posição

            CALENDARIO1.setTime(formato.parse(dadosDeUmDia[0])); //aqui a linha 0 que corresponde à posição da data, converte a String em data
            matrizDasDatas[linha] = dadosDeUmDia[0]; //guarda na posição "linha" apenas o dado referente à data que se encontra na posição 0
             matrizDosNumerosDosDiasDaSemana[linha] = CALENDARIO1.get(Calendar.DAY_OF_WEEK); //a partir da variável calendário é conhecido o dia da semana referente à data sendo guardado no array

            for (int coluna = 0; coluna < QUANTIDADE_DE_COLUNAS_DOS_DADOS; coluna++) {
                matrizDosDados[linha][coluna] = Integer.parseInt(dadosDeUmDia[coluna + 1]); //a partir da coluna 1 (pois a coluna 0 é referente às datas) é guardada toda a informação referente aos dados covid na matriz dos dados
            }
            linha++; //é incrementado uma unidade na variável linha
        }
        readFromFile.close();

        return linha; //quantidade de linhas ocupadas (datas)

    }

    public static String[] intervaloDeTempo(String[] matrizDasDatas, int linhasOcupadas, SimpleDateFormat formato, String[] args, int di, int df) throws ParseException {

        String[] periodoTempo = new String[2]; //array com as datas limitadoras do período pretendido
        boolean validacao = false; //este boolean é falso até que as datas sejam válidas

        if (di == -1 && df == -1) {
            while (!validacao) { //enquanto a validacao não for true o ciclo repete-se pedindo ao utilizador que reescreva as datas
                System.out.print("Data Inicial - ");
                periodoTempo[0] = read.next();
                System.out.print("Data Final - ");
                periodoTempo[1] = read.next();
                validacao = verificacaoDasDatas(periodoTempo[0], periodoTempo[1], matrizDasDatas, linhasOcupadas, formato); //verifica se as datas estão no documento CSV e se a data do fim é mais recente que a dá início, caso seja verdade o boolean fica true
            }
        } else {
            periodoTempo[0] = args[di + 1];
            periodoTempo[1] = args[df + 1];
        }

        return periodoTempo; //retorna as datas limitadoras após serem validadas

    }

    public static boolean verificacaoDasDatas(String dataInicial, String dataFinal, String[] matrizDasDatas, int linhasOcupadas, SimpleDateFormat formato) throws ParseException { //verificar se as datas inseridas pelo utilizador estão no documento e se a inicial vem antes da data final

        int contador = 0; //contador auxiliar para perceber se as duas datas estão presentes no documento CSV

        for (int linha = 0; linha < linhasOcupadas; linha++) { //percorre o array das datas

            if (dataInicial.equals(dataFinal) && dataInicial.equals(matrizDasDatas[linha])) {
                contador = 3;
            } else if (dataInicial.equals(matrizDasDatas[linha]) || dataFinal.equals(matrizDasDatas[linha])) { //ver se este período pertence ao documento CSV
                contador++; //incrementa uma unidade quando pelo menos uma das duas datas está no documento
            }

        }

        if (contador < 2) { //caso o contador seja menor que 2 então uma das datas não está no documento ou são o mesmo dia
            System.out.println("pelo menos uma das datas inseridas e ivalida. verifique se a data que inseriu existe, está no formato correto e digite novamente");
            return false;
        } else if (contador == 2) {
            Date data1 = formato.parse(dataInicial);
            Date data2 = formato.parse(dataFinal); //as duas datas são convertidas para o formato data no formato da variável com o mesmo nome para poderem ser comparadas na linha seguinte
            return data1.before(data2); //caso a data2 (dataFinal) seja mais recente que a data1 (dataInicial) então é retornado true, caso contrário é retornado falso
        } else { //quando entra aqui significa que as duas datas são iguais
            return true;
        }

    }


    public static int[] posicaoDataInicialEFinal(String[] listaDasDatas, String dataInicial, String dataFinal, int linhasOcupadas) {

        int[] posicaoDasDatas = new int[2];

        for (int linha = 0; linha < linhasOcupadas; linha++) {

            if (dataInicial.equals(listaDasDatas[linha])) {
                posicaoDasDatas[0] = linha;
            }
            if (dataFinal.equals(listaDasDatas[linha])) {
                posicaoDasDatas[1] = linha;
            }

        }

        return posicaoDasDatas;

    }


    public static boolean existeSemanaCompleta(int[] listaDosNumerosDosDiasDaSemana, String[] listaDasDatas, String dataInicial, String dataFinal, int linhasOcupadas) {

        boolean existeUmaSegunda = false;
        int[] posicaoDaDataInicialFinal = posicaoDataInicialEFinal(listaDasDatas, dataInicial, dataFinal, linhasOcupadas);

        int linha = posicaoDaDataInicialFinal[0],
                numeroDiaDaSemana = -1,
                posicaoDataFinal = posicaoDaDataInicialFinal[1],
                posicaoSegundaFeira = -1;

        while (numeroDiaDaSemana != 2 && linha <= posicaoDataFinal) {

            numeroDiaDaSemana = listaDosNumerosDosDiasDaSemana[linha];
            posicaoSegundaFeira = linha;
            if (numeroDiaDaSemana == 2) {
                existeUmaSegunda = true;
            }
            linha++;

        }

        if (!existeUmaSegunda) {
            return false;
        } else {
            return posicaoDataFinal - posicaoSegundaFeira >= 6;
        }

    }


    public static int[] posicoesIntervaloSemanasCompletas(int linhasOcupadas, String[] listaDasDatas, int[] listaDosNumerosDosDiasDaSemana, String dataInicial, String dataFinal) {

        int[] posicaoDaDataInicialFinal = posicaoDataInicialEFinal(listaDasDatas, dataInicial, dataFinal, linhasOcupadas),
                posicaointervaloSemanasCompletas = new int[2];

        int linhaDoInicioDasSemanas = -1,
                contador = posicaoDaDataInicialFinal[0],
                linhaDoFimDasSemanas = -1,
                posicaoInicioSemana,
                posicaoFinalSemana;

        while (linhaDoInicioDasSemanas != 2) {

            linhaDoInicioDasSemanas = listaDosNumerosDosDiasDaSemana[contador];
            contador++;

        }

        posicaoInicioSemana = contador - 1;
        contador = posicaoDaDataInicialFinal[1] - 6;

        while (linhaDoFimDasSemanas != 1) {

            linhaDoFimDasSemanas = listaDosNumerosDosDiasDaSemana[contador];
            contador++;

        }

        posicaoFinalSemana = contador - 1;
        posicaointervaloSemanasCompletas[0] = posicaoInicioSemana;
        posicaointervaloSemanasCompletas[1] = posicaoFinalSemana;

        return posicaointervaloSemanasCompletas;

    }


    public static void analiseSemanasCompletas(char escolhaDoTipoDeDadosAcumuladosOuTotais, char escolhaDoTipoDeFicheiro, int linhasOcupadas, String[] listaDasDatas, int[][] listaDosDados, int[] listaDosNumerosDosDiasDaSemana, String dataInicial, String dataFinal, short escolha, File ficheiroDosDadosGuardados, String[] args) throws IOException {

        int[] posicaoInicialFinal = posicoesIntervaloSemanasCompletas(linhasOcupadas, listaDasDatas, listaDosNumerosDosDiasDaSemana, dataInicial, dataFinal);

        int posicaoDaDataInicial = posicaoInicialFinal[0],
                posicaoDaDataFinal = posicaoInicialFinal[1],
                cont = 0,
                linha = 0,
                diasEntreAsDatas = posicaoDaDataFinal - (posicaoDaDataInicial + 1),
                totalNumeroSemanas = (diasEntreAsDatas / 7) + 1,
                posicaoInicioSemana = -1;

        int[][] dadosDeCadaSemana = new int[totalNumeroSemanas][5];

        String[][] datasIniciaisFinaisDasSemanas = new String[totalNumeroSemanas][2];

        for (int posicao = posicaoDaDataInicial; posicao <= posicaoDaDataFinal; posicao++) {

            if (cont == 0) {

                datasIniciaisFinaisDasSemanas[linha][0] = listaDasDatas[posicao];
                cont++;
                posicaoInicioSemana = posicao;

            } else if (cont == 6) {
                datasIniciaisFinaisDasSemanas[linha][1] = listaDasDatas[posicao];
                if (escolhaDoTipoDeFicheiro == 'a') {
                    if (escolhaDoTipoDeDadosAcumuladosOuTotais == 'h') {
                        dadosDeCadaSemana[linha] = analiseDosDadosAcumuladosEmTotais(listaDosDados, posicaoInicioSemana, posicao);
                    } else {
                        dadosDeCadaSemana[linha] = analiseDosDadosAcumuladosEmAcumulados(listaDosDados, posicao);
                    }
                } else {
                    if (escolhaDoTipoDeDadosAcumuladosOuTotais == 'h') {
                        dadosDeCadaSemana[linha] = analiseDosDadosTotaisEmTotais(posicaoInicioSemana, posicao, listaDosDados);
                    } else {
                        dadosDeCadaSemana[linha] = analiseDosDadosTotaisEmAcumulados(posicao, listaDosDados);
                    }
                }
                cont = 0;
                linha++;

            } else {
                cont++;
            }
        }
        String textoIndicador = "Análise Semanal";
        String textoIndicadorCSV = "Analise Semanal";
        String impressaoDosDados1 = "Inicio de Semana   Fim de Semana     NaoInfetados        Infetados   Hospitalizados    InternadosUCI           Obitos";
        String impressaoDosDados2 = "Inicio de Semana   Fim de Semana   Dado Analisado";
        String impressaoDosDadosNoCSV1 = "Inicio de Semana,Fim de Semana,NaoInfetados,Infetados,Hospitalizados,InternadosUCI,Obitos";
        String impressaoDosDadosNoCSV2 = "Inicio de Semana,Fim de Semana,Dado Analisado";

        mostrarAnaliseEGuardarOuNaoMatrizDuplaDupla(textoIndicador, textoIndicadorCSV, impressaoDosDados1, impressaoDosDados2, impressaoDosDadosNoCSV1, impressaoDosDadosNoCSV2, datasIniciaisFinaisDasSemanas, args, dadosDeCadaSemana, escolha, totalNumeroSemanas, ficheiroDosDadosGuardados);
    }


    public static boolean guardarDados(String[] args) {
        char escolha;

        if (args.length == 0) {
            System.out.print("Deseja guardar estes dados num ficheiro CSV? [S/N] \n Opção que pretende escolher: ");
            escolha = read.next().charAt(0);

            while (escolha != 'S' && escolha != 'N') {

                System.out.println("Opcão invalida. Tente novamente");
                System.out.print("Opção que pretende escolher: ");
                escolha = read.next().charAt(0);
            }

        } else {
            escolha = 'S';
        }


        return escolha == 'S';

    }

    public static boolean existeMesesCompletos(String[] listaDasDatas, int linhasOcupadas, SimpleDateFormat formato, String[] periodoTempo) throws ParseException {

        boolean existeOprimeiroDiaDoMes = false;
        int[] posicaoDasDatas = posicaoDataInicialEFinal(listaDasDatas, periodoTempo[0], periodoTempo[1], linhasOcupadas); //tras o número da linha em que se encontra a data inicial e final

        CALENDARIO2.setTime(formato.parse(listaDasDatas[posicaoDasDatas[1]]));

        int mesDataFinal = CALENDARIO2.get(Calendar.MONTH),
                diaDataFinal = CALENDARIO2.get(Calendar.DAY_OF_MONTH),
                totalDiasDoMes = CALENDARIO2.getActualMaximum(Calendar.DAY_OF_MONTH),
                diaDoMes = -1,
                linha = posicaoDasDatas[0],
                mesDoDia1 = 0;

        while (diaDoMes != 1 && linha <= posicaoDasDatas[1]) {

            CALENDARIO1.setTime(formato.parse(listaDasDatas[linha]));
            diaDoMes = CALENDARIO1.get(Calendar.DAY_OF_MONTH);
            mesDoDia1 = CALENDARIO1.get(Calendar.MONTH);
            linha++;

            if (diaDoMes == 1) {
                existeOprimeiroDiaDoMes = true;
            }

        }

        if (!existeOprimeiroDiaDoMes) {
            return false;
        } else {
            if (totalDiasDoMes == diaDataFinal) {
                return true;
            } else {
                return mesDataFinal - mesDoDia1 != 0;
            }
        }

    }

    public static String[] intervaloMesesCompletos(int linhasOcupadas, String[] listaDasDatas, String[] periodoTempo, SimpleDateFormat formato) throws ParseException {

        int[] posicaoDasDatas = posicaoDataInicialEFinal(listaDasDatas, periodoTempo[0], periodoTempo[1], linhasOcupadas);
        String dataInicioMes, dataFinalMes;
        String[] intervaloMesesCompletos = new String[2];

        CALENDARIO2.setTime(formato.parse(listaDasDatas[posicaoDasDatas[1]]));

        int totalDiasDoMes = CALENDARIO2.getActualMaximum(Calendar.DAY_OF_MONTH), //variável que guarda o último dia do mês da segunda data colocada
                diaDoMesFinal = CALENDARIO2.get(Calendar.DAY_OF_MONTH), //variável que guarda o dia do mês da segunda data inserida
                linhaDoInicioDosMeses = -1,
                contador,
                linhaDoFimDosMeses = -1;

        contador = posicaoDasDatas[0];

        while (linhaDoInicioDosMeses != 1) {

            CALENDARIO2.setTime(formato.parse(listaDasDatas[contador]));
            linhaDoInicioDosMeses = CALENDARIO2.get(Calendar.DAY_OF_MONTH);
            contador++;

        }

        dataInicioMes = listaDasDatas[contador - 1];
        contador = posicaoDasDatas[1] - totalDiasDoMes + 1;

        while (linhaDoFimDosMeses != totalDiasDoMes) {

            CALENDARIO2.setTime(formato.parse(listaDasDatas[contador]));
            linhaDoFimDosMeses = CALENDARIO2.get(Calendar.DAY_OF_MONTH);
            contador++;

        }

        dataFinalMes = listaDasDatas[contador - 1];
        Date dataFinalDoMes = formato.parse(dataFinalMes), dataFinal = formato.parse(listaDasDatas[posicaoDasDatas[1]]);

        if (dataFinalDoMes.after(dataFinal)) {
            intervaloMesesCompletos[1] = listaDasDatas[posicaoDasDatas[1] - diaDoMesFinal]; //<---------
        } else {
            intervaloMesesCompletos[1] = dataFinalMes;
        }

        intervaloMesesCompletos[1] = listaDasDatas[posicaoDasDatas[1] - diaDoMesFinal];
        intervaloMesesCompletos[0] = dataInicioMes;

        return intervaloMesesCompletos;

    }

    public static void analiseMesesCompletos(char escolhaDoTipoDeFicheiro, char escolhaAucumladoTotal, int linhasOcupadas, String[] listaDasDatas, int[][] listaDosDados, String[] periodoTempo, SimpleDateFormat formato, short escolhaDoTipoDeDadosaAnalisar, File ficheiroDosDadosGuardados, String[] args) throws IOException, ParseException {

        String[] intervaloMesesCompletos = intervaloMesesCompletos(linhasOcupadas, listaDasDatas, periodoTempo, formato);
        int[] posicaoInicialFinal = posicaoDataInicialEFinal(listaDasDatas, intervaloMesesCompletos[0], intervaloMesesCompletos[1], linhasOcupadas);

        CALENDARIO1.setTime(formato.parse(intervaloMesesCompletos[0]));
        CALENDARIO2.setTime(formato.parse(intervaloMesesCompletos[1]));

        int posicaoDaDataInicial = posicaoInicialFinal[0],
                posicaoDaDataFinal = posicaoInicialFinal[1],
                linha = 0,
                anoDaData1 = CALENDARIO1.get(Calendar.YEAR),
                anoDaData2 = CALENDARIO2.get(Calendar.YEAR),
                mesDaData1 = CALENDARIO1.get(Calendar.MONTH),
                mesDaData2 = CALENDARIO2.get(Calendar.MONTH),
                anosEntreDatas = anoDaData2 - anoDaData1,
                totalNumeroMeses,
                posicaoDia1 = -1;

        if (anosEntreDatas == 0) {
            totalNumeroMeses = (mesDaData2 - mesDaData1 + 1);
        } else {
            totalNumeroMeses = mesDaData2 - mesDaData1 + 1 + 12 * anosEntreDatas;
        }

        int[][] dadosDeCadaMes = new int[totalNumeroMeses][5];
        String[][] datasIniciaisFinaisDosMeses = new String[totalNumeroMeses][2];

        for (int posicao = posicaoDaDataInicial; posicao <= posicaoDaDataFinal; posicao++) {
            CALENDARIO1.setTime(formato.parse(listaDasDatas[posicao]));
            int diaDaData = CALENDARIO1.get(Calendar.DAY_OF_MONTH);
            int diaFinalDoMesDaData = CALENDARIO1.getActualMaximum(Calendar.DAY_OF_MONTH);
            if (diaDaData == 1) {

                posicaoDia1 = posicao;
                datasIniciaisFinaisDosMeses[linha][0] = listaDasDatas[posicao];

            } else if (diaDaData == diaFinalDoMesDaData) {

                datasIniciaisFinaisDosMeses[linha][1] = listaDasDatas[posicao];
                if (escolhaDoTipoDeFicheiro == 'a') {
                    if (escolhaAucumladoTotal == 'h') {
                        dadosDeCadaMes[linha] = analiseDosDadosAcumuladosEmTotais(listaDosDados, posicaoDia1, posicao);
                    } else {
                        dadosDeCadaMes[linha] = analiseDosDadosAcumuladosEmAcumulados(listaDosDados, posicao);
                    }
                } else {
                    if (escolhaAucumladoTotal == 'h') {
                        dadosDeCadaMes[linha] = analiseDosDadosTotaisEmTotais(posicaoDia1, posicao, listaDosDados);
                    } else {
                        dadosDeCadaMes[linha] = analiseDosDadosTotaisEmAcumulados(posicao, listaDosDados);
                    }

                }
                linha++;
            }
        }

        String textoIndicador = "Análise Mensal";
        String textoIndicadorCSV = "Analise Mensal";
        String impressaoDosDados1 = "   Inicio de Mes      Fim de Mes     NaoInfetados        Infetados   Hospitalizados    InternadosUCI           Obitos";
        String impressaoDosDados2 = "   Inicio de Mes      Fim de Mes   Dado Analisado";
        String impressaoDosDadosNoFicheiroCSV1 = "Inicio de Mes,Fim de Mes,NaoInfetados,Infetados,Hospitalizados,InternadosUCI,Obitos";
        String impressaoDosDadosNoFicheiroCSV2 = "Inicio de Mes,Fim de Mes,Dado Analisado";

        mostrarAnaliseEGuardarOuNaoMatrizDuplaDupla(textoIndicador, textoIndicadorCSV, impressaoDosDados1, impressaoDosDados2, impressaoDosDadosNoFicheiroCSV1, impressaoDosDadosNoFicheiroCSV2, datasIniciaisFinaisDosMeses, args, dadosDeCadaMes, escolhaDoTipoDeDadosaAnalisar, totalNumeroMeses, ficheiroDosDadosGuardados);
    }


    public static String[] datasPertencentesAoIntervalo(String[] listaDasDatas, String[] intervaloDeTempo, int linhasOcupadas) {

        int[] posicaoInicialFinal = posicaoDataInicialEFinal(listaDasDatas, intervaloDeTempo[0], intervaloDeTempo[1], linhasOcupadas);

        int posicaoInicial = posicaoInicialFinal[0],
                posicaoFinal = posicaoInicialFinal[1],
                totalDias = posicaoFinal - (posicaoInicial - 1),
                contador = 0;

        String[] datas = new String[totalDias];

        for (int linha = posicaoInicial; linha <= posicaoFinal; linha++) {
            datas[contador] = listaDasDatas[linha];
            contador++;
        }

        return datas;

    }


    public static int[][] analiseDiaria(String[] listaDasDatas, String[] periodoTempo, int[][] listaDosDados, int linhasOcupadas, char escolhaDoTipoDeFicheiro, char escolhaDoTipoDeDadosAcumuladosOuTotais) {
        int[][] analiseDiaria;
        int[] posicaoDasDatas = posicaoDataInicialEFinal(listaDasDatas, periodoTempo[0], periodoTempo[1], linhasOcupadas);
        int linhasTotaisEntreDatas, linha, adicao = 0;
        if (posicaoDasDatas[0] != 0 || !(escolhaDoTipoDeFicheiro == 'a' && escolhaDoTipoDeDadosAcumuladosOuTotais == 'h')) {
            linhasTotaisEntreDatas = (posicaoDasDatas[1] - posicaoDasDatas[0] + 1);
            linha = 0;
            analiseDiaria = new int[linhasTotaisEntreDatas][5];
        } else {
            linhasTotaisEntreDatas = (posicaoDasDatas[1] - posicaoDasDatas[0] + 1);
            linha = 1;
            adicao = 1;
            analiseDiaria = new int[linhasTotaisEntreDatas][5];

            analiseDiaria[0][0] = 0;
            analiseDiaria[0][1] = 0;
            analiseDiaria[0][2] = 0;
            analiseDiaria[0][3] = 0;
            analiseDiaria[0][4] = 0;
        }
        for (int posicao = posicaoDasDatas[0] + adicao; posicao <= posicaoDasDatas[1]; posicao++) {
            if (escolhaDoTipoDeFicheiro == 'a') {
                if (escolhaDoTipoDeDadosAcumuladosOuTotais == 'h') {
                    analiseDiaria[linha] = analiseDosDadosAcumuladosEmTotais(listaDosDados, posicao - 1, posicao);
                } else {
                    analiseDiaria[linha] = analiseDosDadosAcumuladosEmAcumulados(listaDosDados, posicao);
                }
            } else {
                if (escolhaDoTipoDeDadosAcumuladosOuTotais == 'h') {
                    analiseDiaria[linha] = analiseDosDadosTotaisEmTotais(posicao, posicao, listaDosDados);
                } else {
                    analiseDiaria[linha] = analiseDosDadosTotaisEmAcumulados(posicao, listaDosDados);
                }
            }
            linha++;
        }
        return analiseDiaria;
    }

    public static void escreverMatrizDoubleSingular(double[] matrizSimples) {
        System.out.println("Dias até atingir o estado de Óbito");
        System.out.println("     NaoInfetados        Infetados   Hospitalizados    InternadosUCI");
        for (int coluna = 0; coluna < 4; coluna++) {
            System.out.printf("%17.1f", matrizSimples[coluna]);
        }
        System.out.println();
    }

    public static void escreverMatrizStringSingularInteiroDupla(String[] matrizDatas, int[][] matrizDosDados,
                                                                int linhasOcupadas) {

        for (int linhas = 0; linhas < linhasOcupadas; linhas++) {

            System.out.printf("%16s", matrizDatas[linhas]);
            for (int colunas = 0; colunas < 5; colunas++) {
                System.out.printf("%17d", matrizDosDados[linhas][colunas]);
            }

            System.out.println();

        }

    }

    public static void escreverMatrizStringSingularInteiroSingular(String[] matrizDatas, int[][] matrizDosDados,
                                                                   int linhasOcupadas, short escolha) {

        for (int linha = 0; linha < linhasOcupadas; linha++) {

            System.out.printf("%16s", matrizDatas[linha]);
            System.out.printf("%17d", matrizDosDados[linha][escolha]);
            System.out.println();

        }

    }

    public static void escreverMatrizStringDuplainteiroDupla(String[][] matrizDatas, int[][] matrizDosDados,
                                                             int linhasOcupadas) {

        for (int linha = 0; linha < linhasOcupadas; linha++) {

            for (int coluna = 0; coluna < 2; coluna++) {

                System.out.printf("%16s", matrizDatas[linha][coluna]);

            }

            for (int coluna = 0; coluna < QUANTIDADE_DE_COLUNAS_DOS_DADOS; coluna++) {

                System.out.printf("%17d", matrizDosDados[linha][coluna]);

            }

            System.out.println();
        }

    }

    public static void escreverMatrizStringDuplainteiroSingular(String[][] matrizDatas, int[][] matrizDosDados,
                                                                int linhasOcupadas, short escolha) {

        for (int linha = 0; linha < linhasOcupadas; linha++) {

            for (int coluna = 0; coluna < 2; coluna++) {

                System.out.printf("%16s", matrizDatas[linha][coluna]);

            }

            System.out.printf("%17d", matrizDosDados[linha][escolha]);
            System.out.println();

        }

    }


    public static void guardarEscreverMartizDoubleSingular(double[] matrizSimples, File ficheiroDosDadosGuardados) throws IOException {
        FileWriter writeOnFile = new FileWriter((ficheiroDosDadosGuardados), true); // este true serve para nao apagar o que foi escrito no ficheiro anteriormente
        writeOnFile.write("Dias ate atingir o estado de Obito\n");
        writeOnFile.write("NaoInfetados,Infetados,Hospitalizados,InternadosUCI\n");

        for (int colunas = 0; colunas < 4; colunas++) {
            if (colunas != 3) {
                writeOnFile.write(String.format(Locale.US, "%.1f,", matrizSimples[colunas]));
            } else {
                writeOnFile.write(String.format(Locale.US, "%.1f", matrizSimples[colunas]));
            }
        }
        writeOnFile.close();
    }

    public static void guardarEscreverMatrizStringSingularInteiroDuplo(String[] matrizDatas, int[][] matrizDosDados,
                                                                       int linhasOcupadas, File ficheiroDosDadosGuardados) throws IOException {

        FileWriter writeOnFile = new FileWriter(ficheiroDosDadosGuardados, true); // este true serve para nao apagar o que foi escrito no ficheiro anteriormente
        writeOnFile.write("Analise Diaria\n");
        writeOnFile.write("Data,NaoInfetados,Infetados,Hospitalizados,InternadosUCI,Obitos\n");

        for (int linhas = 0; linhas < linhasOcupadas; linhas++) {

            writeOnFile.write(matrizDatas[linhas] + ",");

            for (int colunas = 0; colunas < 5; colunas++) {

                if (colunas != 4) {
                    writeOnFile.write(matrizDosDados[linhas][colunas] + ",");
                } else {
                    writeOnFile.write(matrizDosDados[linhas][colunas] + "");
                }

            }
            writeOnFile.write("\n");
        }

        writeOnFile.close();

    }

    public static void guardarEscreverMatrizStringSingularInteiroSingular(String[] matrizDatas,
                                                                          int[][] matrizDosDados, int linhasOcupadas, short escolha, File ficheiroDosDadosGuardados) throws IOException {

        FileWriter writeOnFile = new FileWriter((ficheiroDosDadosGuardados), true); // este true serve para nao apagar o que foi escrito no ficheiro anteriormente
        writeOnFile.write("Analise Diaria");
        writeOnFile.write("Data,Dado Analisado");

        for (int linha = 0; linha < linhasOcupadas; linha++) {

            writeOnFile.write(matrizDatas[linha] + ",");
            writeOnFile.write(matrizDosDados[linha][escolha] + "");
            writeOnFile.write("\n");

        }

        writeOnFile.close();

    }

    public static void guardarEscrevermatrizStringinteirosDuplaVirgulas(String textoIndicadorCSV, String textoIdentificador, String[][]
            matrizDatas, int[][] matrizDosDados, int linhasOcupadas, File ficheiroDosDadosGuardados) throws IOException {

        FileWriter writeOnFile = new FileWriter((ficheiroDosDadosGuardados), true); // este true serve para nao apagar o que foi escrito no ficheiro anteriormente
        writeOnFile.write(textoIndicadorCSV + "\n");
        writeOnFile.write(textoIdentificador + "\n");

        for (int linha = 0; linha < linhasOcupadas; linha++) {

            for (int coluna = 0; coluna < 2; coluna++) {

                if (coluna != 1) {
                    writeOnFile.write(matrizDatas[linha][coluna] + ",");
                } else {
                    writeOnFile.write(matrizDatas[linha][coluna]);
                }

            }

            for (int coluna = 0; coluna < QUANTIDADE_DE_COLUNAS_DOS_DADOS; coluna++) {

                if (coluna != QUANTIDADE_DE_COLUNAS_DOS_DADOS - 1) {
                    writeOnFile.write(matrizDosDados[linha][coluna] + ",");
                } else {
                    writeOnFile.write(matrizDosDados[linha][coluna] + "");
                }

            }

            writeOnFile.write("\n");

        }

        writeOnFile.close();

    }

    public static void guardarEscrevermatrizStringinteirosSingularVirgulas(String textoIndicadorCSV, String textoIdentificador, String[][]
            matrizDatas, int[][] matrizDosDados, int linhasOcupadas, short escolha, File ficheiroDosDadosGuardados) throws IOException {

        FileWriter writeOnFile = new FileWriter((ficheiroDosDadosGuardados), true);
        writeOnFile.write(textoIndicadorCSV + "\n");
        writeOnFile.write(textoIdentificador + "\n");

        for (int linha = 0; linha < linhasOcupadas; linha++) {

            for (int coluna = 0; coluna < 2; coluna++) {

                writeOnFile.write(matrizDatas[linha][coluna] + ",");

            }

            writeOnFile.write(matrizDosDados[linha][escolha] + "");
            writeOnFile.write("\n");

        }

        writeOnFile.close();

    }

    public static int[] analiseDosDadosAcumuladosEmAcumulados(int[][] listaDosDados, int posicao) {
        int[] dadosAcumulados = new int[5];
        dadosAcumulados[0] = listaDosDados[posicao][0];
        dadosAcumulados[1] = listaDosDados[posicao][1];
        dadosAcumulados[2] = listaDosDados[posicao][2];
        dadosAcumulados[3] = listaDosDados[posicao][3];
        dadosAcumulados[4] = listaDosDados[posicao][4];

        return dadosAcumulados;
    }

    public static int[] analiseDosDadosAcumuladosEmTotais(int[][] listaDosDados, int posicaoInicial, int posicaoFinal) {

        int hospitalizadosFinal,
                internadosUCIFinal,
                obitosFinal,
                totalHospitalizados,
                totalInternadosUCI,
                totalObitos;

        int[] dadosAcumulados = new int[5];

        hospitalizadosFinal = listaDosDados[posicaoFinal][2];
        internadosUCIFinal = listaDosDados[posicaoFinal][3];
        obitosFinal = listaDosDados[posicaoFinal][4];
        totalHospitalizados = hospitalizadosFinal - listaDosDados[posicaoInicial][2];
        totalInternadosUCI = internadosUCIFinal - listaDosDados[posicaoInicial][3];
        totalObitos = obitosFinal - listaDosDados[posicaoInicial][4];
        dadosAcumulados[0] = listaDosDados[posicaoFinal][0];
        dadosAcumulados[1] = listaDosDados[posicaoFinal][1];
        dadosAcumulados[2] = totalHospitalizados;
        dadosAcumulados[3] = totalInternadosUCI;
        dadosAcumulados[4] = totalObitos;

        return dadosAcumulados;

    }

    public static int[] analiseDosDadosTotaisEmTotais(int posicaoInicial, int posicaoFinal, int[][] listaDosDados) {

        int totalNaoInfetado = listaDosDados[posicaoFinal][0],
                totalInfetado = listaDosDados[posicaoFinal][1],
                totalHospitalizados = 0,
                totalInternadosUCI = 0,
                totalObitos = 0;

        int[] dadosTotais = new int[5];

        for (int linha = posicaoInicial; linha <= posicaoFinal; linha++) {
            totalHospitalizados += listaDosDados[linha][2];
            totalInternadosUCI += listaDosDados[linha][3];
            totalObitos += listaDosDados[linha][4];
        }

        dadosTotais[0] = totalNaoInfetado;
        dadosTotais[1] = totalInfetado;
        dadosTotais[2] = totalHospitalizados;
        dadosTotais[3] = totalInternadosUCI;
        dadosTotais[4] = totalObitos;

        return dadosTotais;
    }

    public static int[] analiseDosDadosTotaisEmAcumulados(int posicaoFinal, int[][] listaDosDados) {
        int acumuladoNaoInfetado = listaDosDados[posicaoFinal][0],
                acumuladoInfetado = listaDosDados[posicaoFinal][1],
                acumuladoHospitalizados = 0,
                acumuladoInternadosUCI = 0,
                acumuladoObitos = 0;

        int[] dadosAcumulados = new int[5];

        for (int linha = 0; linha <= posicaoFinal; linha++) {
            acumuladoHospitalizados += listaDosDados[linha][2];
            acumuladoInternadosUCI += listaDosDados[linha][3];
            acumuladoObitos += listaDosDados[linha][4];
        }

        dadosAcumulados[0] = acumuladoNaoInfetado;
        dadosAcumulados[1] = acumuladoInfetado;
        dadosAcumulados[2] = acumuladoHospitalizados;
        dadosAcumulados[3] = acumuladoInternadosUCI;
        dadosAcumulados[4] = acumuladoObitos;

        return dadosAcumulados;
    }


    public static double[][] lerMatrizDasProbabilidades(File matrizTransicao) throws IOException {

        Scanner readFromFile = new Scanner(matrizTransicao);

        double[][] lerMatrizProbabilidadeDeTransicao = new double[QUANTIDADE_DE_COLUNAS_DOS_DADOS][QUANTIDADE_DE_COLUNAS_DOS_DADOS];
        for (int linha = 0; linha < QUANTIDADE_DE_COLUNAS_DOS_DADOS; linha ++){
            for (int coluna = 0; coluna < QUANTIDADE_DE_COLUNAS_DOS_DADOS; coluna ++){
                if (readFromFile.hasNext()) {
                    lerMatrizProbabilidadeDeTransicao[linha][coluna] = Double.parseDouble(readFromFile.next().split("=")[1]);
                }else{
                    coluna --;
                }
            }
        }
        readFromFile.close();
        return lerMatrizProbabilidadeDeTransicao;
    }


    public static String lerEVerificarDataParaPrevisao(SimpleDateFormat formato, String[] args, int linhaT) {
        String dataInserida = "";
        boolean validacaoData = false;

        while (!validacaoData) {

            if (linhaT == -1) {
                System.out.print("Data - ");
                dataInserida = read.next();
            } else {
                dataInserida = args[linhaT + 1];
            }

            try {
                formato.setLenient(false);
                formato.parse(dataInserida);
                validacaoData = true;
            } catch (ParseException ignored) {

            }
            if (!validacaoData) {
                System.out.print("Data Inválida! Volte a inserir: ");
            }
        }

        return dataInserida;
    }

    public static int verificarSeADataEstaNoDocumento(String[] listaDasDatas, int linhasOcupadas, String dataInserida) {
        int posicaoDaData = -1;

       for (int linha = 0; linha < linhasOcupadas; linha++) {
            if (listaDasDatas[linha].equals(dataInserida)) {
                posicaoDaData = linha;
            }
        }
        if (posicaoDaData == -1) {
            posicaoDaData = linhasOcupadas;
        } else if (posicaoDaData == 0){
            posicaoDaData = 1;
        } //caso a pessoa escolha a primeira data do documento a posicaoDaData fica igual a 1 sendo retornado o 0 apresentado os dados desse dia

        return posicaoDaData - 1;
    }


    public static void matrizDosDadosPrevistosParaUmDeterminadoDia(SimpleDateFormat formato, int[][] listaDosDados, String[] listaDasDatas, int linhasOcupadas, double[][] matrizDasTransicoes, String dataInserida, File ficheiroDosDadosGuardados, String[] args) throws ParseException, IOException {

        final int posicaoUltimaLinha = verificarSeADataEstaNoDocumento(listaDasDatas, linhasOcupadas, dataInserida);

        final int tamanhoDaMatriz = 5;

        Date dataFinalDoDocumento = formato.parse(listaDasDatas[posicaoUltimaLinha]),
                dataParaPrevisao = formato.parse(dataInserida);

        long diferencaEmMilisegundos = Math.abs(dataParaPrevisao.getTime() - dataFinalDoDocumento.getTime()),
                diferencaEntreAsDatas = TimeUnit.DAYS.convert(diferencaEmMilisegundos, TimeUnit.MILLISECONDS);

        int[] ultimoDadoDaLista = listaDosDados[posicaoUltimaLinha];

        double soma;

        double[] previsaoDoDia = new double[5];

        double[][] matrizAuxiliar1,
                matrizAuxiliar2 = matrizDasTransicoes;

        for (int linha = 0; linha < diferencaEntreAsDatas - 1; linha++) {
            matrizAuxiliar1 = produtoMatrizes(matrizAuxiliar2, matrizDasTransicoes, tamanhoDaMatriz);
            matrizAuxiliar2 = matrizAuxiliar1;
        }

        for (int linha = 0; linha < QUANTIDADE_DE_COLUNAS_DOS_DADOS; linha++) {
            soma = 0;
            for (int coluna = 0; coluna < QUANTIDADE_DE_COLUNAS_DOS_DADOS; coluna++) {
                soma += matrizAuxiliar2[linha][coluna] * ultimoDadoDaLista[coluna];
            }
            previsaoDoDia[linha] = soma;
        }

        String textoIndicador = "Previsão para um Determinado Dia";
        String textoIndicadorCSV = "Previsao para um Determinado Dia";
        String textoInformativo1 = "            Data     NaoInfetados        Infetados   Hospitalizados    InternadosUCI           Obitos";
        String textoInformativo2 = "Data,NaoInfetados,Infetados,Hospitalizados,InternadosUCI,Obitos";
        escreverArrayUnidimensionalStringDouble(textoIndicador, dataInserida, previsaoDoDia, textoInformativo1, QUANTIDADE_DE_COLUNAS_DOS_DADOS);
        if (guardarDados(args)) {
            guardarEscreverArrayUnidimensionalStringDouble(textoIndicadorCSV, dataInserida, previsaoDoDia, textoInformativo2, QUANTIDADE_DE_COLUNAS_DOS_DADOS, ficheiroDosDadosGuardados);
        }
    }

    public static void escreverArrayUnidimensionalStringDouble(String textoIndicador, String data, double[] dados, String texto, int tamanho) {

        System.out.println(textoIndicador);
        System.out.println(texto);
        System.out.printf("%16s", data);
        for (int coluna = 0; coluna < tamanho; coluna++) {
            System.out.printf("%17.1f", dados[coluna]);
        }
        System.out.println();
    }

    public static void guardarEscreverArrayUnidimensionalStringDouble(String textoIndicadorCSV, String data, double[] dados, String texto, int tamanho, File ficheiroDosDadosGuardados) throws IOException {

        FileWriter writeOnFile = new FileWriter((ficheiroDosDadosGuardados), true); // este true serve para nao apagar o que foi escrito no ficheiro anteriormente
        writeOnFile.write(textoIndicadorCSV + "\n");
        writeOnFile.write(texto + "\n");
        writeOnFile.write(data + ",");
        for (int coluna = 0; coluna < tamanho; coluna++) {
            if (coluna != tamanho - 1) {
                writeOnFile.write(String.format(Locale.US, "%.1f,", dados[coluna]));
            }else{
                writeOnFile.write(String.format(Locale.US, "%.1f", dados[coluna]));
            }
        }
        writeOnFile.write("\n");
        writeOnFile.close();
    }

    public static double[][] matrizDasProbabilidadesSemOsObitos(File matrizTransicao) throws IOException { //É retirada a linha e a coluna 5 da matriz das probabilidades
        int totalLinhas = 4,
                totalColunas = 4;

        double[][] matrizProbabilidadesSemObitos = new double[totalLinhas][totalColunas];

        for (int linha = 0; linha < totalLinhas; linha++) {

            for (int coluna = 0; coluna < totalColunas; coluna++) {

                matrizProbabilidadesSemObitos[linha][coluna] = lerMatrizDasProbabilidades(matrizTransicao)[linha][coluna]; //é chamado o método que lê a matriz das probabilidades para ser guardada sem a coluna e linha 5

            }
        }
        return matrizProbabilidadesSemObitos;
    }


    public static double[][] criacaoMatrizIdentidadeDeOrdem4() { //aqui é criada uma matriz identidade ("uns" na diagonal) de ordem 4
        final int TAMANHO = 4;

        double[][] matrizIdentidade = new double[TAMANHO][TAMANHO];

        for (int posicao = 0; posicao < TAMANHO; posicao++) {
            matrizIdentidade[posicao][posicao] = 1;
        }

        return matrizIdentidade;

    }


    public static double[][] subtrairMatrizes4Por4(double[][] matriz1, double[][] matriz2) { //neste método é feita a subtrção da identidade pela matriz das probabilidades sem a coluna e linha 5

        final int TAMANHO = 4;
        double[][] matrizResultadoSubtracao = new double[TAMANHO][TAMANHO];

        for (int linha = 0; linha < TAMANHO; linha++) {
            for (int coluna = 0; coluna < TAMANHO; coluna++) {
                matrizResultadoSubtracao[linha][coluna] = matriz2[linha][coluna] - matriz1[linha][coluna]; //matriz2=identidade mtriz1=matriz das probabilidades reduzida
            }
        }
        return matrizResultadoSubtracao;

    }

    public static double[][] criacaoDaMatrizLU(double[][] matrizAFaturar, String matrizUouL) {

        final int TAMANHO = 4;
        double[][] matrizFatoracaoL = new double[TAMANHO][TAMANHO],
                matrizFatoracaoU;

        matrizFatoracaoU = criacaoMatrizIdentidadeDeOrdem4();

        for (int i = 0; i < TAMANHO; i++) {
            for (int j = i; j < TAMANHO; j++) {
                double soma = 0;
                for (int k = 0; k < i; k++) {
                    soma += matrizFatoracaoL[j][k] * matrizFatoracaoU[k][i];
                }
                matrizFatoracaoL[j][i] = matrizAFaturar[j][i] - soma;
            }

            for (int j = i + 1; j < TAMANHO; j++) {
                double soma = 0;
                for (int k = 0; k < i; k++) {
                    soma += matrizFatoracaoL[i][k] * matrizFatoracaoU[k][j];
                }
                matrizFatoracaoU[i][j] = (matrizAFaturar[i][j] - soma) / matrizFatoracaoL[i][i];
            }
        }

        if (matrizUouL.equals("MatrizL")) {
            return matrizFatoracaoL;
        } else {
            return matrizFatoracaoU;
        }
    }

    public static double[] fazerColunaInversaDoL(double[][] matrizFatoracaoL, int colunaAAnalisar) {

        final int TAMANHO = 4;

        double[] valoresDaColunaEMAnalise = new double[TAMANHO];

        double primeiroValor = 0,
                segundoValor = 0,
                terceiroValor = 0,
                quartoValor = 0;

        switch (colunaAAnalisar) {
            case 0 -> {
                primeiroValor = 1 / matrizFatoracaoL[0][0];
                segundoValor = -(matrizFatoracaoL[1][0] * primeiroValor) / matrizFatoracaoL[1][1];
                terceiroValor = -(matrizFatoracaoL[2][0] * primeiroValor + matrizFatoracaoL[2][1] * segundoValor) / matrizFatoracaoL[2][2];
                quartoValor = -(matrizFatoracaoL[3][0] * primeiroValor + matrizFatoracaoL[3][1] * segundoValor + matrizFatoracaoL[3][2] * terceiroValor) / matrizFatoracaoL[3][3];
            }
            case 1 -> {
                primeiroValor = 0;
                segundoValor = 1 / matrizFatoracaoL[1][1];
                terceiroValor = -(matrizFatoracaoL[2][1] * segundoValor) / matrizFatoracaoL[2][2];
                quartoValor = -(matrizFatoracaoL[3][1] * segundoValor + matrizFatoracaoL[3][2] * terceiroValor) / matrizFatoracaoL[3][3];
            }
            case 2 -> {
                primeiroValor = 0;
                segundoValor = 0;
                terceiroValor = 1 / matrizFatoracaoL[2][2];
                quartoValor = -(matrizFatoracaoL[3][2] * terceiroValor) / matrizFatoracaoL[3][3];
            }
            case 3 -> {
                primeiroValor = 0;
                segundoValor = 0;
                terceiroValor = 0;
                quartoValor = 1 / matrizFatoracaoL[3][3];
            }
        }
        valoresDaColunaEMAnalise[0] = primeiroValor;
        valoresDaColunaEMAnalise[1] = segundoValor;
        valoresDaColunaEMAnalise[2] = terceiroValor;
        valoresDaColunaEMAnalise[3] = quartoValor;

        return valoresDaColunaEMAnalise;
    }

    public static double[][] fazerAInversaDoL(double[][] matrizFatoracaoL) {

        final int TAMANHO = 4;

        double[][] inversaDoL = new double[TAMANHO][TAMANHO];

        for (int coluna = 0; coluna < TAMANHO; coluna++) {
            double[] valoresDaColuna = fazerColunaInversaDoL(matrizFatoracaoL, coluna);
            for (int linha = 0; linha < TAMANHO; linha++) {
                inversaDoL[linha][coluna] = valoresDaColuna[linha];
            }
        }
        return inversaDoL;
    }

    public static double[][] fazerAInversaDoU(double[][] matrizU) {

        double[][] inversaDoU = criacaoMatrizIdentidadeDeOrdem4();

        inversaDoU[0][1] = -matrizU[0][1];
        inversaDoU[1][2] = -matrizU[1][2];
        inversaDoU[2][3] = -matrizU[2][3];
        inversaDoU[0][2] = -(matrizU[0][1] * inversaDoU[1][2] + matrizU[0][2]);
        inversaDoU[1][3] = -(matrizU[1][2] * inversaDoU[2][3] + matrizU[1][3]);
        inversaDoU[0][3] = -(matrizU[0][1] * inversaDoU[1][3] + matrizU[0][2] * inversaDoU[2][3] + matrizU[0][3]);

        return inversaDoU;
    }


    public static double[] vetorUnitario() {
        final int TAMANHO = 4;

        double[] vetor = new double[TAMANHO];

        for (int coluna = 0; coluna < TAMANHO; coluna++) {
            vetor[coluna] = 1;
        }
        return vetor;
    }

    public static void numeroDeDiasAteAtingirOEstadoDeObito(String[] args, File ficheiroDosDadosGuardados, File matrizTransicao) throws IOException {

        final int TAMANHO = 4;

        double[][] matrizIdentidade = criacaoMatrizIdentidadeDeOrdem4(),
                matrizQ = matrizDasProbabilidadesSemOsObitos(matrizTransicao),
                diferencaDeImenosQ = subtrairMatrizes4Por4(matrizQ, matrizIdentidade),
                matrizdeFaturacaoU = criacaoDaMatrizLU(diferencaDeImenosQ, "MatrizU"),
                matrizdeFaturacaoL = criacaoDaMatrizLU(diferencaDeImenosQ, "MatrizL"),
                inversaMatrizFaturadaU = fazerAInversaDoU(matrizdeFaturacaoU),
                inversaMatrizFaturadaL = fazerAInversaDoL(matrizdeFaturacaoL),
                multiplicaoDasInversasLU = produtoMatrizes(inversaMatrizFaturadaU, inversaMatrizFaturadaL, TAMANHO);

        double[] vetorUnitario = vetorUnitario();
        double[] resultadoFinal = produtoDeMatrizDuplaComSimples(multiplicaoDasInversasLU, vetorUnitario);

        escreverMatrizDoubleSingular(resultadoFinal);

        if (guardarDados(args)) {
            guardarEscreverMartizDoubleSingular(resultadoFinal, ficheiroDosDadosGuardados);
        }

    }

    public static double[] produtoDeMatrizDuplaComSimples(double[][] matrizDupla, double[] matrizSimples) {

        double soma;
        final int TAMANHO = 4;
        double[] matrizResultante = new double[TAMANHO];

        for (int linha = 0; linha < TAMANHO; linha++) {
            soma = 0;
            for (int coluna = 0; coluna < TAMANHO; coluna++) {
                soma += matrizSimples[coluna] * matrizDupla[coluna][linha];
            }
            matrizResultante[linha] = soma;
        }
        return matrizResultante;
    }

    public static double[][] produtoMatrizes(double[][] matriz1, double[][] matriz2, int tamanho) {

        double[][] matrizProduto = new double[tamanho][tamanho];

        for (int linha = 0; linha < tamanho; linha++) {
            for (int coluna = 0; coluna < tamanho; coluna++) {
                double soma = 0;
                for (int i = 0; i < tamanho; i++) {
                    double produto = matriz1[linha][i] * matriz2[i][coluna];
                    soma += produto;
                }
                matrizProduto[linha][coluna] = soma;
            }
        }

        return matrizProduto;
    }


    public static boolean[] testagemDosMetodos() throws FileNotFoundException, ParseException {

        final short quantidadeDeMetodosATestar = 12;
        boolean[] matrizValidacao = new boolean[quantidadeDeMetodosATestar];
        SimpleDateFormat formatoDataTeste1 = new SimpleDateFormat("yyyy-MM-dd");


        //verificação do metodo da leitura do ficheiro com o retorno das linhas ocupadas (lerFicheiro)
        String[] listaDasDatasTeste = new String[QUANTIDADE_DE_LINHAS]; // array das datas
        int[][] listaDosDadosTeste = new int[QUANTIDADE_DE_LINHAS][QUANTIDADE_DE_COLUNAS_DOS_DADOS];
        int[] listaDosNumerosDosDiasDaSemanaTeste = new int[QUANTIDADE_DE_LINHAS];
        matrizValidacao[0] = lerFicheiro(listaDasDatasTeste, listaDosDadosTeste, listaDosNumerosDosDiasDaSemanaTeste, FICHEIRODETESTEACUMULADOS, new SimpleDateFormat("yyyy-MM-dd")) == 61;

        String[] listaDasDatasTeste2 = new String[QUANTIDADE_DE_LINHAS]; // array das datas
        int[][] listaDosDadosTeste2 = new int[QUANTIDADE_DE_LINHAS][QUANTIDADE_DE_COLUNAS_DOS_DADOS];
        int[] listaDosNumerosDosDiasDaSemanaTeste2 = new int[QUANTIDADE_DE_LINHAS];
        int linhasOcupadasTotais = lerFicheiro(listaDasDatasTeste2, listaDosDadosTeste2, listaDosNumerosDosDiasDaSemanaTeste2, FICHEIRODETESTETOTAIS, new SimpleDateFormat("dd-MM-yyyy"));

        //verificação do metodo da validação das datas inseridas pelo utilizador (verificacaoDasDatas)
        boolean testeDatas1 = verificacaoDasDatas("2020-04-01", "2020-05-11", listaDasDatasTeste, 61, formatoDataTeste1);
        boolean testeDatas2 = verificacaoDasDatas("2020-04-12", "2020-04-05", listaDasDatasTeste, 61, formatoDataTeste1);
        matrizValidacao[1] = testeDatas1 && !testeDatas2;

        //verficação do metodo da posicao da data inicial e final no array da lista das datas (posicaoDataInicialEFinal)
        int[] posicaoDasDatas1 = posicaoDataInicialEFinal(listaDasDatasTeste, "2020-04-10", "2020-05-15", 61);
        boolean testePosicaoDasDatas1 = posicaoDasDatas1[0] == 9 && posicaoDasDatas1[1] == 44;
        int[] posicaoDasDatas2 = posicaoDataInicialEFinal(listaDasDatasTeste, "2020-04-03", "2020-05-26", 61);
        boolean testePosicaoDasDatas2 = posicaoDasDatas2[0] == 2 && posicaoDasDatas2[1] == 55;
        matrizValidacao[2] = testePosicaoDasDatas1 && testePosicaoDasDatas2;

        //verificação de pelo menos uma semana completa no intervalo inserido pelo utilizador (existeSemanaCompleta)
        boolean testeExisteSemanaCompleta1 = existeSemanaCompleta(listaDosNumerosDosDiasDaSemanaTeste, listaDasDatasTeste, "2020-04-01", "2020-04-10", 61);
        boolean testeExisteSemanaCompleta2 = existeSemanaCompleta(listaDosNumerosDosDiasDaSemanaTeste, listaDasDatasTeste, "2020-04-01", "2020-05-10", 61);
        matrizValidacao[3] = !testeExisteSemanaCompleta1 && testeExisteSemanaCompleta2;

        //verificação do metodo do intervalo das semanas completas do intervalo inserido pelo utilizador (samanasCompletas)
        int[] intervaloSemanasCompletas1 = posicoesIntervaloSemanasCompletas(61, listaDasDatasTeste, listaDosNumerosDosDiasDaSemanaTeste, "2020-04-06", "2020-04-12");
        boolean testeSemanasCompletas1 = intervaloSemanasCompletas1[0] == 5 && intervaloSemanasCompletas1[1] == 11;
        int[] intervaloSemanasCompletas2 = posicoesIntervaloSemanasCompletas(61, listaDasDatasTeste, listaDosNumerosDosDiasDaSemanaTeste, "2020-04-03", "2020-05-15");
        boolean testeSemanasCompletas2 = intervaloSemanasCompletas2[0] == 5 && intervaloSemanasCompletas2[1] == 39;
        matrizValidacao[4] = testeSemanasCompletas1 && testeSemanasCompletas2;

        //verificação de pelo menos um mês completo no intervalo inserido pelo utilizador (existeMesesCompletos)
        String[] periodotempoTeste1 = new String[]{"2020-04-01", "2020-04-30"};
        boolean testeExisteMesCompleto1 = existeMesesCompletos(listaDasDatasTeste, 61, formatoDataTeste1, periodotempoTeste1);
        String[] periodotempoTeste2 = new String[]{"2020-04-10", "2020-05-31"};
        boolean testeExisteMesCompleto2 = existeMesesCompletos(listaDasDatasTeste, 61, formatoDataTeste1, periodotempoTeste2);
        String[] periodotempoTeste3 = new String[]{"2020-04-10", "2020-05-19"};
        boolean testeExisteMesCompleto3 = existeMesesCompletos(listaDasDatasTeste, 61, formatoDataTeste1, periodotempoTeste3);
        matrizValidacao[5] = testeExisteMesCompleto1 && testeExisteMesCompleto2 && !testeExisteMesCompleto3;

        //verificação das datas que pertencem ao intervalo inserido pelo utilizador (datasPertencentesAoIntervalo)
        String[] intervaloParaVerificacaoDasDatasQuePertencemAoIntervalo = new String[]{"2020-04-02", "2020-04-08"};
        String[] datasPertencentesAoIntervalo = datasPertencentesAoIntervalo(listaDasDatasTeste, intervaloParaVerificacaoDasDatasQuePertencemAoIntervalo, 61);
        boolean testeDasDatasQuePertencemAoIntervalo = datasPertencentesAoIntervalo[2].equals("2020-04-04") && datasPertencentesAoIntervalo[4].equals("2020-04-06");
        matrizValidacao[6] = testeDasDatasQuePertencemAoIntervalo;

        //verificação da análise diária (analiseDiaria)
        String[] intervaloParaVerificacaoDaAnaliseDiaria = new String[]{"2020-04-03", "2020-04-09"};
        int[][] analiseDiaria = analiseDiaria(listaDasDatasTeste, intervaloParaVerificacaoDaAnaliseDiaria, listaDosDadosTeste, 61, 'a', 'h');
        boolean testeDaDaAnaliseDiaria = analiseDiaria[0][0] == 544 && analiseDiaria[2][1] == 754 && analiseDiaria[5][3] == 245;
        matrizValidacao[7] = testeDaDaAnaliseDiaria;

        //verificação da analiseDosDadosAcumuladosEmTotais
        int[] posicaoDasDatasParaVerificacaoDaAnaliseDosDadosAcumuladosEmTotais = posicaoDataInicialEFinal(listaDasDatasTeste, "2020-05-11", "2020-05-20", 61);
        int[] analiseDosDadosAcumuladosEmTotais = analiseDosDadosAcumuladosEmTotais(listaDosDadosTeste, posicaoDasDatasParaVerificacaoDaAnaliseDosDadosAcumuladosEmTotais[0], posicaoDasDatasParaVerificacaoDaAnaliseDosDadosAcumuladosEmTotais[1]);
        boolean testeAnaliseDosDadosAcumuladosEmTotais = analiseDosDadosAcumuladosEmTotais[0] == 2605 && analiseDosDadosAcumuladosEmTotais[1] == 1981 && analiseDosDadosAcumuladosEmTotais[2] == 5926 && analiseDosDadosAcumuladosEmTotais[3] == 958 && analiseDosDadosAcumuladosEmTotais[4] == 119;
        matrizValidacao[8] = testeAnaliseDosDadosAcumuladosEmTotais;

        //verificação da analiseDosDadosTotaisEmTotais
        int[] posicaoDasDatasParaVerificacaoDaAnaliseDosDadosTotaisEmTotais = posicaoDataInicialEFinal(listaDasDatasTeste2, "26-11-2020", "29-11-2020", linhasOcupadasTotais);
        int[] analiseDosDadosTotaisEmTotais = analiseDosDadosTotaisEmTotais(posicaoDasDatasParaVerificacaoDaAnaliseDosDadosTotaisEmTotais[0], posicaoDasDatasParaVerificacaoDaAnaliseDosDadosTotaisEmTotais[1], listaDosDadosTeste2);
        boolean testeAnaliseDosDadosTotaisEmTotais = analiseDosDadosTotaisEmTotais[0] == 10078877 && analiseDosDadosTotaisEmTotais[1] == 80838 && analiseDosDadosTotaisEmTotais[2] == 12800 && analiseDosDadosTotaisEmTotais[3] == 2107 && analiseDosDadosTotaisEmTotais[4] == 300;
        matrizValidacao[9] = testeAnaliseDosDadosTotaisEmTotais;

        //verificação da analiseDosDadosTotaisEmAcumulados
        int[] posicaoDasDatasParaVerificacaoDaAnaliseDosDadosTotaisEmAcumulados = posicaoDataInicialEFinal(listaDasDatasTeste2, "03-11-2020", "05-11-2020", linhasOcupadasTotais);
        int[] analiseDosDadosTotaisEmAcumulados = analiseDosDadosTotaisEmTotais(posicaoDasDatasParaVerificacaoDaAnaliseDosDadosTotaisEmAcumulados[0], posicaoDasDatasParaVerificacaoDaAnaliseDosDadosTotaisEmAcumulados[1], listaDosDadosTeste2);
        boolean testeAnaliseDosDadosTotaisEmAcumulados = analiseDosDadosTotaisEmAcumulados[0] == 10095344 && analiseDosDadosTotaisEmAcumulados[1] == 67157 && analiseDosDadosTotaisEmAcumulados[2] == 11425 && analiseDosDadosTotaisEmAcumulados[3] == 1543 && analiseDosDadosTotaisEmAcumulados[4] == 233;
        matrizValidacao[10] = testeAnaliseDosDadosTotaisEmAcumulados;

        //verificação se a data está no documento se sim é considerada a data anterior para efeitos do cálculo da previsão
        boolean testeVerificarSeADataEstaNoDocumento = verificarSeADataEstaNoDocumento(listaDasDatasTeste2, linhasOcupadasTotais, "18-11-2020") == 16;
        matrizValidacao[11] = testeVerificarSeADataEstaNoDocumento;


        return matrizValidacao;

    }

}